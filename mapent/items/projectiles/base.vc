/**********************************************************************************
 * Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC
 * Copyright (c) 2010, Moloch
 * Copyright (c) 2018, Ketmar Dark
 *
 * This file is part of Spelunky.
 *
 * You can redistribute and/or modify Spelunky, including its source code, under
 * the terms of the Spelunky User License.
 *
 * Spelunky is distributed in the hope that it will be entertaining and useful,
 * but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.
 *
 * The Spelunky User License should be available in "Game Information", which
 * can be found in the Resource Explorer, or as an external file called COPYING.
 * If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>
 *
 **********************************************************************************/
// projectiles
class ItemProjectile : MapItem abstract;

bool launchedByPlayer = true;


// can this item harm the entity?
// entity flags like `invincible` is checked elsewhere,
// here we should only do item-specific checks
bool canHarm (MapEntity e) {
  //(bounceCount > 0 && fabs(yVel) < 2.5 && fabs(xVel) < 2) return false; // bounced arrow didn't hit anyone
  return (bounceCount == 0 || fabs(yVel) >= 2.5 || fabs(xVel) >= 2);
}


// return `true` to stop further processing
override bool onAfterSomethingHit () {
  instanceRemove();
  return true; // stop it, we are dead anyway
}


override bool onExplosionTouch (MapObject xplo) {
  if (invincible) return false;
  ::onExplosionTouch(xplo);
  instanceRemove();
  return true;
}


override bool doPlayerColAction (PlayerPawn plr) {
  if (safe || nudged) return false; // continue
  if (plr.dead) return false;
  if (!canHarm(plr)) return false;
  if (global.plife > 0) {
    global.plife -= damage;
    if (!plr.dead && global.plife <= 0 /*&& isRealLevel()*/) level.addDeath(objName);
  }
  plr.xVel = xVel;
  plr.yVel = -4;
  plr.spillBlood();
  plr.playSound('sndHurt');
  plr.stunned = true;
  plr.stunTimer = 20;
  instanceRemove();
  return true; // stop
}


defaultproperties {
  canHitEnemies = true;
  sellingToShopAllowed = true;
}
