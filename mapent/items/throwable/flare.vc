/**********************************************************************************
 * Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC
 * Copyright (c) 2010, Moloch
 * Copyright (c) 2018, Ketmar Dark
 *
 * This file is part of Spelunky.
 *
 * You can redistribute and/or modify Spelunky, including its source code, under
 * the terms of the Spelunky User License.
 *
 * Spelunky is distributed in the hope that it will be entertaining and useful,
 * but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.
 *
 * The Spelunky User License should be available in "Game Information", which
 * can be found in the Resource Explorer, or as an external file called COPYING.
 * If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>
 *
 **********************************************************************************/
// flare
class ItemFlare['oFlare'] : ItemThrowable;

int sparkTimer = 1;
int prevFrame = -1;


override bool initialize () {
  if (!::initialize()) return false;
  setSprite('sFlare');
  return true;
}


override bool onFellInWater (MapTile water) {
  ::onFellInWater(water); // this does splash
  instanceRemove();
  return true;
}


override void thinkFrame () {
  if (sparkTimer > 0) {
    if (--sparkTimer == 0) {
      level.MakeMapObject(ix+global.randOther(0, 3)-global.randOther(0, 3), iy-4+global.randOther(0, 3)-global.randOther(0, 3), 'oFlareSpark');
      sparkTimer = 2;
    }
  }

  ::thinkFrame();

  if (prevFrame != trunci(imageFrame)) {
    prevFrame = trunci(imageFrame);
    lightRadius = 96+global.randOther(-6, 6);
    if (level.loserGPU) lightRadius = 96;
  }

  if (heldBy) {
    auto web = MapItem(level.isObjectAtPoint(ix, iy, &level.cbIsObjectWeb));
    if (web) web.life -= 50;
  }
}


defaultproperties {
  objName = 'Flare';
  desc = "Flare";
  desc2 = "A chunk of magical fire. Water will extinguish it.";
  setCollisionBounds(-4, -4, 4, 4);
  xVel = 0;
  yVel = 0;
  grav = 0.6;
  myGrav = 0.6;
  invincible = true;
  breaksOnCollision = false;
  bounce = true;
  lightRadius = 96;
  depth = 30;
  imageSpeed = 0.3;

  activeWhenHeld = true; // for animation
  spectralWhenHeld = true;
}
