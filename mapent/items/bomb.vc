/**********************************************************************************
 * Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC
 * Copyright (c) 2010, Moloch
 * Copyright (c) 2018, Ketmar Dark
 *
 * This file is part of Spelunky.
 *
 * You can redistribute and/or modify Spelunky, including its source code, under
 * the terms of the Spelunky User License.
 *
 * Spelunky is distributed in the hope that it will be entertaining and useful,
 * but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.
 *
 * The Spelunky User License should be available in "Game Information", which
 * can be found in the Resource Explorer, or as an external file called COPYING.
 * If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>
 *
 **********************************************************************************/
// bomb
class ItemBomb['oBomb'] : MapItem;

MapObject stickedTo;
//bool sticky;
//bool armed;
int stickyXDiff, stickyYDiff;

int alarmCount;


override bool initialize () {
  if (!::initialize()) return false;
  setSprite('sBomb');
  return true;
}


final void setSticky (bool asticky) {
  sticky = asticky;
  setSprite(asticky ? 'sBomb' : 'sBomb');
}


override bool onExplosionTouch (MapObject xplo) {
  if (invincible) return false;
  if (heldBy) return false;
  if (armed && alarmCount > 8) armed = false;
  armIt(global.randOther(4, 8));
  imageSpeed = 1;
  if (iy < xplo.iy) yVel = -global.randOther(2, 4);
  xVel = global.randOther(2, 4)*(ix < xplo.ix ? -1 : 1);
  return true;
}


void explode () {
  level.MakeMapObject(ix, iy, 'oExplosion');
}


// return `true` to stop player from throwing it
override bool onTryUseItem (PlayerPawn plr) {
  if (!armed) {
    global.bombs = max(0, global.bombs-1);
    armIt(80);
    //imageSpeed = 0.2; //k8: this is default
    resaleValue = 0;
    return true; // don't autothrow it
  }
  return false; // autothrow it
}


override void thinkFrame () {
  ::thinkFrame();
  if (!isInstanceAlive) return;

  // stick to enemy
  if (sticky && !stickedTo && (fabs(xVel) > 2 || fabs(yVel) > 2)) {
    auto obj = level.isObjectInRect(ix-2, iy-1, 5, 5, delegate bool (MapObject o) {
      if (o.spectral || !o.active || o.heldBy || o.dead) return false;
      auto e = MapEnemy(o);
      if (e) {
        if (e.stunned || e.status >= STATE_STUNNED || e.status == THROWN || e.dead) return false;
      }
      // one object should not have more than one bomb sticked onto it
      foreach (ItemBomb bb; level.objGrid.allObjects(ItemBomb)) if (bb.stickedTo == o) return false;
      return true;
    }, castClass:MapEnemy);

    if (obj) {
      stickedTo = obj;
      stickyXDiff = obj.ix-ix;
      stickyYDiff = obj.iy-iy;
    }
  }

  lightRadius = (armed ? 32+(alarmCount > 40 ? global.randOther(-4, 4) : global.randOther(-6, 6)) : 0);
  if (level.loserGPU) lightRadius = (armed ? 32 : 0);

  if (alarmCount > 0) {
    if (--alarmCount == 0) {
      if (!armed) {
        armIt();
      } else {
        explode();
        if (heldBy) {
          //FIXME: callback!
          heldBy.holdItem = none;
          //if (oCharacter) oCharacter.holdItem = 0;
        }
        instanceRemove();
        return;
      }
    }
    if (armed) {
      setSprite('sBombArmed');
      imageSpeed = (alarmCount < 40 ? 1.0 : 0.2);
    }
  }

  if (!heldBy) {
    auto spr = getSprite();
    if (spr.Name == 'sBombArmed') depth = 49;
    if (sticky) depth = 1;

    if (armed && !heldBy && !global.config.optShopkeeperIdiots && level.isInShop(ix/16, iy/16)) {
      level.scrShopkeeperAnger(GameLevel::SCAnger.BombDropped, /*96*/128, self);
    }
  }

  // step end: sticky
  if (stickedTo) {
    setXY(stickedTo.fltx-stickyXDiff, stickedTo.flty-stickyYDiff);
  }
}


void armIt (optional int activationTime) {
  if (!armed) {
    if (!specified_activationTime) activationTime = 40;
    setSprite('sBombArmed');
    armed = true;
    imageSpeed = (activationTime < 40 ? 1.0 : 0.2);
    alarmCount = max(0, activationTime);
    active = true;
  }
}


defaultproperties {
  objName = 'Bomb';
  desc = "Bomb";
  desc2 = "A powerful explosive demolition device. It can be turned on living creatures to great effect.";
  setCollisionBounds(-4, -4, 4, 4);
  //breaksOnCollision = false;
  canPickUp = true;
  breakPieces = false;
  activeWhenHeld = true;
  sellingToShopAllowed = true;
  imageSpeed = 0;
  depth = 58;
}


// ////////////////////////////////////////////////////////////////////////// //
// explosion
class MapObjExplosion['oExplosion'] : MapObject;

transient SpriteImage smask;
name smaskName;
bool createFlame = true;
bool suicide;


/*
override int width () { return max(1, ::width()); }
override int height () { return max(1, ::height()); }
*/


override void onLoaded () {
  ::onLoaded();
  if (smaskName) smask = level.sprStore[smaskName];
}


override bool initialize () {
  if (!::initialize()) return false;
  setSprite('sExplosion');
  if (smaskName) smask = level.sprStore[smaskName];
  // mask: sExplosionMask
  //!damage = global.explosionDmg;
  playSound('sndExplosion');
  level.scrShake(5);
  return true;
}


override void onAnimationLooped () {
  //scrCreateFlame(ix, iy, 3);
  instanceRemove();
}


override void thinkFrame () {
  if (createFlame) {
    createFlame = false;
    scrCreateFlame(ix, iy, 3);
  }
       if (imageFrame <= 1) lightRadius = 64;
  else if (imageFrame <= 5) lightRadius = 64+roundi(16*(imageFrame-1));
  else lightRadius = 128*roundi(8-imageFrame);
  doExplosion();
  if (imageLoopCount > 0) instanceRemove();
}


void doExplosion () {
  int flen = smask.frames.length;
  if (flen < 1) return;
  auto frm = smask.frames[trunci(imageFrame)%flen];
  level.touchEntitiesWithMask(ix-frm.xofs, iy-frm.yofs, frm, delegate bool (MapEntity e) {
    e.onExplosionTouch(self);
    return false; // go on
  });
}


defaultproperties {
  objName = 'Explosion';
  desc = "";
  desc2 = "";
  spectral = true;
  imageSpeed = 0.8;
  smaskName = 'sExplosionMask';
  depth = -1;
}
