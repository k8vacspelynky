/**********************************************************************************
 * Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC
 * Copyright (c) 2010, Moloch
 * Copyright (c) 2018, Ketmar Dark
 *
 * This file is part of Spelunky.
 *
 * You can redistribute and/or modify Spelunky, including its source code, under
 * the terms of the Spelunky User License.
 *
 * Spelunky is distributed in the hope that it will be entertaining and useful,
 * but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.
 *
 * The Spelunky User License should be available in "Game Information", which
 * can be found in the Resource Explorer, or as an external file called COPYING.
 * If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>
 *
 **********************************************************************************/
class ItemDice['oDice'] : MapItem;

enum RollState {
  None, // ready to roll
  Rolling, // rolled
  Finished, // landed in shop
  Failed, // landed outside of the shop
}
RollState rollState;

bool pickedOutsideOfAShop;


override bool initialize () {
  if (!::initialize()) return false;
  setSprite('sDice1');
  value = global.randOther(1, 6);
  return true;
}


// 0: failed roll
final int getRollNumber () {
  if (rollState != RollState.Finished) return 0;
  return value;
}


final void resetRollState () {
  rollState = RollState.None;
}


bool isReadyToThrowForBet () {
  if (!forSale) return false;
  return ((rollState == RollState.Failed || rollState == RollState.None) && level.player.bet);
}


bool isInsideCrapsShop () {
  if (!level.isInShop(ix/16, iy/16)) return false;
  auto sst = level.lg.roomShopType(ix/16, iy/16);
  return (sst == 'Craps');
}


// various side effects
// called only if object was succesfully put into player hands
override void onPickedUp (PlayerPawn plr) {
  pickedOutsideOfAShop = !isInsideCrapsShop();
  if (rollState != RollState.Finished) rollState = RollState.None;
}


override void drawSignsWithOfs (int xpos, int ypos, int scale, float currFrameDelta) {
  if (!forSale) return;
  if ((rollState == RollState.Failed || rollState == RollState.None) && level.player.bet) {
    int xi, yi;
    getInterpCoords(currFrameDelta, scale, out xi, out yi);
    auto spr = level.sprStore['sRedArrowDown'];
    if (spr && spr.frames.length) {
      auto spf = spr.frames[0];
      spf.blitAt(xi-xpos-spf.xofs*scale, yi-ypos-(12+spf.yofs)*scale, scale);
    }
  }
}


override void onCheckItemStolen (PlayerPawn plr) {
  if (!heldBy || pickedOutsideOfAShop) return;
  if (forSale && !level.player.bet && rollState != RollState.Failed) {
    bool inShop = level.isInShop(ix/16, iy/16);
    if (!inShop) {
      level.scrShopkeeperAnger(GameLevel::SCAnger.ItemStolen); // don't steal it!
    }
  }
}


override void thinkFrame () {
  if (forSale && /*!forVending && cost > 0 &&*/ !level.hasAliveShopkeepers(skipAngry:true)) {
    forSale = false;
    cost = 0;
  }

  //lightRadius = max(origLightRadius, (forSale && level.isInShop(ix/16, iy/16) ? 64 : 0));
  if (forSale && level.isInShop(ix/16, iy/16)) {
    lightRadius = max(default.lightRadius, 64);
  } else {
    lightRadius = default.lightRadius;
  }

  if (heldBy) {
    /*
         if (oCharacter.facing == LEFT) x = oCharacter.x - 4;
    else if (oCharacter.facing == RIGHT) x = oCharacter.x + 4;

    if (heavy) {
      if (oCharacter.state == DUCKING and abs(oCharacter.xVel) < 2) y = oCharacter.y; else y = oCharacter.y-2;
    } else {
      if (oCharacter.state == DUCKING and abs(oCharacter.xVel) < 2) y = oCharacter.y+4; else y = oCharacter.y+2;
    }
    depth = 1;

    if (oCharacter.holdItem == 0) held = false;
    */
    // stealing makes shopkeeper angry
    //writeln("!!! fs=", forSale, "; bet=", level.player.bet, "; st=", rollState);
  } else {
    moveRel(xVel, yVel);

    bool colLeft = !!isCollisionLeft(1);
    bool colRight = !!isCollisionRight(1);
    bool colBot = !!isCollisionBottom(1);
    //bool colTop = !!isCollisionTop(1);

    if (!colBot && yVel < 6) yVel += myGrav;

         if (fabs(xVel) < 0.1) xVel = 0;
    else if (colLeft || colRight) xVel = -xVel*0.5;

    if (colBot) {
      // bounce
      if (yVel > 1) { ++bounceCount; yVel = -yVel*bounceFactor; } else yVel = 0;
      // friction
           if (fabs(xVel) < 0.1) xVel = 0;
      else if (fabs(xVel) != 0) xVel *= frictionFactor;
      if (fabs(yVel) < 1) {
        flty -= 1;
        if (!isCollisionBottom(1)) flty += 1;
        yVel = 0;
      }
    }

    if (colLeft) {
      if (!colRight) fltx += 1;
      //yVel = 0;
    } else if (colRight) {
      fltx -= 1;
      //yVel = 0;
    }

    if (isCollisionTop(1)) {
      if (yVel < 0) yVel = -yVel*0.8; else flty += 1;
    }

    //!depth = (global.hasSpectacles ? 0 : 101); //???

    if (isCollisionInRect(ix-3, iy-3, 7, 7, &level.cbCollisionLava)) {
      myGrav = 0;
      xVel = 0;
      yVel = 0;
      shiftY(0.05);
    } else {
      myGrav = 0.6;
    }

    if (isCollisionAtPoint(ix, iy-5, &level.cbCollisionLava)) {
      instanceRemove();
      return;
    }
  }

  if (!isInstanceAlive || spectral) return;

  if (fabs(xVel) > 3 || fabs(yVel) > 3) {
    /*
    auto plr = level.player;
    if (plr.isRectCollision(ix+enemyColX, iy+enemyColY, enemyColW, enemyColH)) {
      doPlayerColAction(plr);
    }
    */
    spectral = true;
    level.forEachObjectInRect(ix-2, iy-2, 5, 5, &doObjectColAction);
    spectral = false;
  }

  // roll states
  if (fabs(yVel) > 2 || fabs(xVel) > 2) {
    setSprite('sDiceRoll');
    if (rollState != RollState.Rolling) {
      //auto ost = value;
      value = global.randOther(1, 6);
      //writeln("DICE: oldval=", ost, "; newval=", value);
    }
    switch (rollState) {
      case RollState.Finished:
        // NO CHEATING!
        if (level.player.bet > 0) level.scrShopkeeperAnger(GameLevel::SCAnger.CrapsCheated);
        break;
      default:
        rollState = RollState.Rolling;
        break;
    }
  } else {
    if (yVel == 0 && fabs(xVel <= 2) && isCollisionBottom(1)) {
      // landed
      switch (rollState) {
        case RollState.Rolling:
          rollState = (isInsideCrapsShop() ? RollState.Finished : RollState.Failed);
          if (rollState == RollState.Finished) level.player.onDieRolled(self);
          break;
        case RollState.Finished:
        case RollState.Failed:
          break;
        case RollState.None:
        default: rollState = RollState.None; break;
      }
    }
  }

  if (rollState != RollState.Rolling) {
    switch (value) {
      case 1: setSprite('sDice1'); break;
      case 2: setSprite('sDice2'); break;
      case 3: setSprite('sDice3'); break;
      case 4: setSprite('sDice4'); break;
      case 5: setSprite('sDice5'); break;
      default: setSprite('sDice6'); break;
    }
  }

  canPickUp = (rollState != RollState.Rolling);
}


defaultproperties {
  objType = 'Dice';
  desc = "Die";
  desc2 = "A six-sided die. The storeowner talks to it every night before he goes to sleep.";
  setCollisionBounds(-6, 0, 6, 8);
  cost = 0;
  heavy = true;
  //rolled = false;
  //rolling = false;
  canBeNudged = true;
  canPickUp = true;
  holdYOfs = -4;
  rollState = RollState.None;
  bloodless = true; // just in case, lol
  canHitEnemies = true;
  sellingToShopAllowed = true;
}
