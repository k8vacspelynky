/**********************************************************************************
 * Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC
 * Copyright (c) 2010, Moloch
 * Copyright (c) 2018, Ketmar Dark
 *
 * This file is part of Spelunky.
 *
 * You can redistribute and/or modify Spelunky, including its source code, under
 * the terms of the Spelunky User License.
 *
 * Spelunky is distributed in the hope that it will be entertaining and useful,
 * but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.
 *
 * The Spelunky User License should be available in "Game Information", which
 * can be found in the Resource Explorer, or as an external file called COPYING.
 * If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>
 *
 **********************************************************************************/
class MonsterTunnelMan['oTunnelMan'] : MapObject;

enum TalkState {
  Silent,
  Hello,
  Bargaining,
  Thanks,
  Complaining,
  ShortcutOpened,
}


TalkState talk = 0;
int donate = 0;
int udHoldCounter = 0;
const int udHoldCounterFirst = 15;
const int udHoldCounterMax = 4;

bool upPressed, downPressed, payPressed;
bool upHold, downHold;
bool playerGoAway;
bool donated;

int helpMessageShown = 0;


override bool initialize () {
  if (!::initialize()) return false;
  setSprite('sTunnelManLeft');
  setCollisionBoundsFromFrame();
  return true;
}


void setInTitle () {
  setSprite('sTunnelManRight');
}


void playerMovedAway () {
  upPressed = false;
  downPressed = false;
  payPressed = false;
  upHold = false;
  downHold = false;
  udHoldCounter = 0;
  //writeln("player moved away; talk=", talk);
  if (talk != TalkState.Silent) {
    if (!donated) talk = TalkState.Complaining;
    //playerGoAway = true;
  }
}


void playerComes () {
  playerMovedAway();
  playerGoAway = false;
  talk = TalkState.Hello;
}


void doTalk () {
  if (talk == TalkState.Hello) {
    level.osdMessageTalk("HEY THERE! I'M THE TUNNEL MAN!\nI DIG SHORTCUTS.", replace:true, timeout:999999, inShopOnly:false);
    if (!helpMessageShown) {
      helpMessageShown = 1;
      level.osdMessage("PRESS $PAY TO TALK.", -666);
    }
  } else if (talk == TalkState.Bargaining) {
    string msg = va("CAN YOU LEND ME A LITTLE MONEY?\nI NEED ~$%d~ FOR A NEW SHORTCUT.\n|DONATE: %d|", level.stats.leftForNextTunnel(), donate);
    level.osdMessageTalk(msg, replace:true, timeout:999999, inShopOnly:false);
    if (helpMessageShown != 2) {
      helpMessageShown = 2;
      level.osdMessage("PRESS $PAY TO PAY.\nPRESS $UP OR $DOWN TO CHANGE DONATION.", -666);
    }
  } else if (talk == TalkState.Thanks) {
    level.osdMessageTalk("THANKS! YOU WON'T REGRET IT!", replace:true, timeout:999999, inShopOnly:false);
  } else if (talk == TalkState.Complaining) {
    level.osdMessageTalk("I'LL NEVER GET THIS SHORTCUT BUILT!", replace:true, timeout:999999, inShopOnly:false);
  } else if (talk == TalkState.ShortcutOpened) {
    level.osdMessageTalk("ONE SHORTCUT, COMING UP!", replace:true, timeout:999999, inShopOnly:false);
  }
}


override void thinkFrame () {
  auto stats = level.stats;

  if (payPressed) {
    payPressed = false;
    if (talk == TalkState.Hello) {
      talk = TalkState.Bargaining;
    } else if (talk == TalkState.Bargaining) {
      if (donate > 0) {
        donated = true;
        bool digged = stats.payForTunnel(donate);
        talk = (digged ? TalkState.ShortcutOpened : TalkState.Thanks);
        stats.takeMoney(donate);
        //global.moneySpent += donate;
      } else {
        talk = TalkState.Complaining;
      }
      playerGoAway = true;
    }
  }

  if (talk == TalkState.Bargaining) {
    if (upPressed || downPressed) {
      udHoldCounter = udHoldCounterFirst;
      donate = min(donate+1000, stats.money);
    } else if (udHoldCounter > 0) {
      if (--udHoldCounter == 0) {
        donate = min(donate+(upHold ? 1000 : downHold ? -1000 : 0), stats.money);
        udHoldCounter = udHoldCounterMax;
      }
    }
  }
}


/*
override void drawWithOfs (int xpos, int ypos, int scale, float currFrameDelta) {
  ::drawWithOfs(xpos, ypos, scale, currFrameDelta);
  if (kissed) {
    auto oclr = GLVideo.color;
    GLVideo.color = 0xff_ff_00;
    level.sprStore.loadFont('sFontSmall');
    //int scale = 3;
    level.sprStore.renderText((ix-32)*scale-xpos, (iy-18)*scale-ypos, "MY HERO!", 3);
    GLVideo.color = oclr;
  }
}
*/


defaultproperties {
  //imageSpeed = 0.5;
  active = true;
  spectral = false;
  depth = 92;
}
