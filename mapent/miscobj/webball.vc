/**********************************************************************************
 * Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC
 * Copyright (c) 2010, Moloch
 * Copyright (c) 2018, Ketmar Dark
 *
 * This file is part of Spelunky.
 *
 * You can redistribute and/or modify Spelunky, including its source code, under
 * the terms of the Spelunky User License.
 *
 * Spelunky is distributed in the hope that it will be entertaining and useful,
 * but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.
 *
 * The Spelunky User License should be available in "Game Information", which
 * can be found in the Resource Explorer, or as an external file called COPYING.
 * If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>
 *
 **********************************************************************************/
// web ball trail
class MapObjYellowTrail['oYellowTrail'] : MapObject;


override bool initialize () {
  if (!::initialize()) return false;
  setSprite('sYellowTrail');
  return true;
}


override void onAnimationLooped () {
  instanceRemove();
}


override void thinkFrame () {
}


defaultproperties {
  depth = 1;
  spectral = true;
  imageSpeed = 1;
}


// ////////////////////////////////////////////////////////////////////////// //
// web ball
class ItemWebBall['oWebBall'] : MapObject;

int life;
int trailTime = -1; //4


override bool initialize () {
  if (!::initialize()) return false;
  setSprite('sWebBall');
  yVel = -(global.randOtherFloat(3)+1);
  xVel = global.randOther(1, 3);
  if (global.randOther(1, 2) == 1) xVel *= -1;
  life = global.randOther(20, 100);
  return true;
}


override void onAnimationLooped () {
  if (spriteLName == 'sWebCreate') {
    auto obj = ItemWeb(level.MakeMapObject(ix-8, iy-8, 'oWeb'));
    if (obj) obj.dying = true;
    instanceRemove();
  }
}


void createWeb () {
  setSprite('sWebCreate');
  xVel = 0;
  yVel = 0;
}


override void thinkFrame () {
  if (--trailTime == 0) {
    level.MakeMapObject(ix, iy, 'oYellowTrail');
    trailTime = default.trailTime;
  }

  shiftXY(xVel, yVel);

  if (yVel < 6) yVel += 0.2;
  // if (yVel &gt; 0) yVel = 0; // in the original

  if (life > 0) life -= 1; else { createWeb(); return; }

  level.checkTilesInRect(x0, y0, width, height, delegate bool (MapTile o) {
    if (!o.collidesWith(self)) return false;
    if (o.spectral || !o.visible || (!o.solid && !o.water)) return false;
    createWeb();
    return true;
  });

  if (spriteLName == 'sWebCreate') return;

  level.isObjectInRect(x0, y0, width, height, delegate bool (MapObject o) {
    if (!o.collidesWith(self)) return false;

    // enemy
    auto enemy = MapEnemy(o);
    if (enemy) {
      if (enemy.dead || enemy isa EnemyGiantSpiderHang) return false;
      createWeb();
      return true;
    }

    // item
    if (o isa MapItem) { createWeb(); return true; }


    return false;
  }, precise:false);
}


defaultproperties {
  objName = 'Web Ball';

  invincible = true;
  bounce = false; // true?
  collectible = false;

  spectral = false;

  imageSpeed = 1; //???

  setCollisionBounds(-8, -8, 7, 7);
}
