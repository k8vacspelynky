/**********************************************************************************
 * Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC
 * Copyright (c) 2010, Moloch
 * Copyright (c) 2018, Ketmar Dark
 *
 * This file is part of Spelunky.
 *
 * You can redistribute and/or modify Spelunky, including its source code, under
 * the terms of the Spelunky User License.
 *
 * Spelunky is distributed in the hope that it will be entertaining and useful,
 * but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.
 *
 * The Spelunky User License should be available in "Game Information", which
 * can be found in the Resource Explorer, or as an external file called COPYING.
 * If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>
 *
 **********************************************************************************/
class EnemyTombLord['oTombLord'] : MapEnemy;

/*
IDLE = 0;
WALK = 1;
TURN = 2;
ATTACK = 3;
STATE_STUNNED = 98;
STATE_DEAD = 99;
*/

bool hasSceptre;
MapObject crate;
int attackTimer;


override void Destroy () {
  delete crate;
  ::Destroy();
}


override bool initialize () {
  if (!::initialize()) return false;
  setSprite('sTombLordLeft', 'sTombLordRight');
  dir = global.randOther(0, 1);
  if (level.isWaterAtPoint(ix, iy)) swimming = true;
  return true;
}


void genCrate () {
  if (!crate) {
    crate = level.MakeMapObject(ix, iy, 'oCrate');
    crate.spectral = true;
    crate.active = false;
    crate.visible = false;
  }
}


override void onAnimationLooped () {
  auto sname = getSprite().Name;
  if (sname == 'sTombLordTurnR') {
    dir = Dir.Right;
    status = WALK;
    setSprite('sTombLordWalkL', 'sTombLordWalkR');
  } else if (sname == 'sTombLordTurnL') {
    dir = Dir.Left;
    status = WALK;
    setSprite('sTombLordWalkL', 'sTombLordWalkR');
  } else if (sname == 'sTombLordAttackL' || sname == 'sTombLordAttackR') {
    status = IDLE;
    counter = 30;
    imageSpeed = 0.25;
  }
}


override bool onTouchedByPlayer (PlayerPawn plr) {
  if (plr.dead || dead || status == STATE_DEAD) return false;
  if ((stunned || status == STATE_STUNNED) && !global.hasSpikeShoes) return false;

  int x = ix, y = iy;
  int plrx = plr.ix, plry = plr.iy;

  if (abs(plrx-(x+16)) > 16) {
    // do nothing
  } else if ((plr.status == JUMPING || plr.status == STATE_FALLING) && plry < y+8 && !plr.swimming) {
    plr.yVel = -6-0.2*plr.yVel;
    if (global.hasSpikeShoes) {
      hp -= 3*trunci(ffloor(plr.fallTimer/16)+1);
      scrCreateBlood(ix+16, iy+24, 1);
    } else {
      hp -= 1*trunci(ffloor(plr.fallTimer/16)+1);
    }
    plr.fallTimer = 0;
    countsAsKill = true;
    level.MakeMapObject(x+16, y, 'oBone');
    plr.playSound('sndHit');
  } else if (plr.invincible == 0) {
    plr.blink = 30;
    plr.invincible = 30;
    if (plry < y) plr.yVel = -6;
    if (plrx < x) plr.xVel = -6; else plr.xVel = 6;
    if (global.plife > 0) {
      global.plife -= damage;
      if (global.plife <= 0 /*and isRealLevel()*/) level.addDeath(objName);
    }
    plr.playSound('sndHurt');
  }

  return false; // don't skip thinker
}


// return `false` to do standard weapon processing
override bool onTouchedByPlayerWeapon (PlayerPawn plr, PlayerWeapon wpn) {
  if (heldBy) return true;
  if (wpn.prestrike) return true;
  if (wpn.iy < iy+12) {
    writeln("TOMB LORD WHIP!");
    wpn.hitEnemy = true;
    hp -= wpn.damage;
    countsAsKill = true;
    scrCreateBlood(ix+16, iy+24, 1);
    plr.playSound('sndHit');
  }
  return true;
}


/*
override void onBulletHit (ObjBullet bullet) {
  if (status == STATE_DEAD) return;
  if (status != STATE_DEAD) {
    status = STATE_STUNNED;
    counter = 20;
    countsAsKill = true;
  }
  ::onBulletHit(bullet);
}
*/


override void thinkFrame () {
  ::thinkFrame();
  if (!isInstanceAlive) return;

  moveRel(xVel, yVel);

  yVel = fmin(yVel+myGrav, yVelLimit);

  int x = ix, y = iy;

  if (level.isSolidAtPoint(x+16, y+16)) hp = 0;

  if (hp < 1) {
    scrCreateBlood(x+14+global.randOther(0, 4), y+14+global.randOther(0, 4), 4);
    foreach (; 0..4) level.MakeMapObject(x+14+global.randOther(0, 4), y+12+global.randOther(0, 6), 'oBone');
    if (hasSceptre) level.MakeMapObject(x+16, y+16, 'oSceptre');
    if (countsAsKill) level.addKill(objName);
    if (crate) {
      crate.spectral = false;
      crate.active = true;
      crate.visible = true;
      crate.fltx = ix+8;
      crate.flty = iy+16;
      crate = none; // release it
    }
    //!!!with (oBossBlock) dying = true;
    instanceRemove();
    return;
  }

  if (isCollisionBottom(1) && status != STATE_STUNNED) yVel = 0;
  if (yVel < 0 && isCollisionTop(1)) yVel = 0; //k8

  if (attackTimer > 0) --attackTimer;

  auto plr = level.player;

  if (status == IDLE) {
    if (counter > 0) --counter;
    if (counter <= 0) status = WALK;
  } else if (status == WALK) {
    if (counter > 0) --counter;
    if (dir == Dir.Left) {
      if (isCollisionLeft(1) || (plr.ix > x+16 && abs(plr.iy-(y+32)) < 16 && counter == 0)) {
        setSprite('sTombLordTurnR');
        status = TURN;
        counter = 30;
      } else if (plr.ix < x+16 && abs(plr.iy-(y+16)) < 32 && attackTimer == 0) {
        status = ATTACK;
        //setSprite('sTombLordAttackL');
        setSprite('sTombLordAttackL', 'sTombLordAttackR');
        imageFrame = 0;
        xVel = 0;
      } else {
        xVel = -1;
      }
    } else if (dir == Dir.Right) {
      if (isCollisionRight(1) || (plr.ix < x+16 && abs(plr.iy-(y+32)) < 16 && counter == 0)) {
        setSprite('sTombLordTurnL');
        dir = Dir.Left;
        status = TURN;
        counter = 30;
      } else if (plr.ix > x+16 && abs(plr.iy-(y+16)) < 32 && attackTimer == 0) {
        status = ATTACK;
        //setSprite('sTombLordAttackR');
        setSprite('sTombLordAttackL', 'sTombLordAttackR');
        imageFrame = 0;
        xVel = 0;
      } else {
        xVel = 1;
      }
    }
  } else if (status == TURN) {
    xVel = 0;
  } else if (status == ATTACK) {
    xVel = 0;
    imageSpeed = 0.5;
    attackTimer = 100;
    if (imageFrame >= 7 && imageFrame <= 12) {
      if (dir == Dir.Left) {
        auto obj = level.MakeMapObject(x+8, y+12+global.randOther(0, 4), 'oFly');
        if (obj) obj.xVel = -global.randOther(3, 5);
      } else {
        auto obj = level.MakeMapObject(x+24, y+12+global.randOther(0, 4), 'oFly');
        if (obj) obj.xVel = global.randOther(3, 5);
      }
    }
  } else if (status >= STATE_STUNNED) {
    status = WALK;
  }

  if (isCollision()) shiftY(-2);

       if (status == WALK) setSprite('sTombLordWalkL', 'sTombLordWalkR');
  else if (status == IDLE) setSprite('sTombLordLeft', 'sTombLordRight');
}


defaultproperties {
  objName = 'Tomb Lord';
  desc = "Tomb Lord";
  desc2 = "The embalmed, reanimated corpse of an ancient necromancer, tasked with guarding these caverns from intruders.";
  status = IDLE;

  setCollisionBounds(6, 0, 26, 32);
  xVel = 2.5;
  imageSpeed = 0.25;

  hp = 20;
  invincible = 0;
  heavy = true;
  bloodless = true;
  damage = 2;
  status = IDLE;

  canPickUp = false;
  bounced = false;
  dead = false;
  counter = 0;
  attackTimer = 0;

  dir = Dir.Right;

  doBasicPhysics = false;
  flying = false; // don't fall

  countsAsKill = false;
  leavesBody = true; // we will do our own death sequence

  depth = 60;
}
