/**********************************************************************************
 * Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC
 * Copyright (c) 2010, Moloch
 * Copyright (c) 2018, Ketmar Dark
 *
 * This file is part of Spelunky.
 *
 * You can redistribute and/or modify Spelunky, including its source code, under
 * the terms of the Spelunky User License.
 *
 * Spelunky is distributed in the hope that it will be entertaining and useful,
 * but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.
 *
 * The Spelunky User License should be available in "Game Information", which
 * can be found in the Resource Explorer, or as an external file called COPYING.
 * If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>
 *
 **********************************************************************************/
class EnemyAlien['oAlien'] : MapEnemy;


override bool initialize () {
  if (!::initialize()) return false;
  setSprite('sAlien');
  dir = global.randOther(0, 1);
  return true;
}


// ////////////////////////////////////////////////////////////////////////// //
override void thinkFrame () {
  ::thinkFrame();
  if (!isInstanceAlive) return;

  moveRel(xVel, yVel);
  //done in basic hanler: if (collision_point(x+8, y+8, oSolid, 0, 0)) hp = 0;

  /+done in basic hanler:
  if (hp &lt; 1) {
    CreateBlood(x+8, y+8, 3);
    if (countsAsKill) {
      if (isRealLevel()) global.enemyKills[15] += 1;
      global.aliens += 1;
      global.kills += 1;
    }
    instance_destroy();
  }
  +/

  yVel = fmin(yVel+0.6, 42.0);

  int x = ix, y = iy;

  if (isCollisionBottom(1) && status != STATE_STUNNED) yVel = 0;

  if (status == IDLE) {
    if (counter > 0) --counter;
    if (counter == 0) {
      dir = global.randOther(0, 1);
      status = WALK;
    }
  } else if (status == WALK) {
    if (isCollisionRight(1)) dir = Dir.Left;
    if (isCollisionLeft(1)) dir = Dir.Right;
    if (dir == Dir.Left && !level.isSolidAtPoint(x-1, y) && !level.isSolidAtPoint(x-1, y+16)) {
      dir = Dir.Right;
    } else if (dir == Dir.Right && !level.isSolidAtPoint(x+16, y) && !level.isSolidAtPoint(x+16, y+16)) {
      dir = Dir.Left;
    }

    if ((!level.isSolidAtPoint(x-1, y+16) || level.isSolidAtPoint(x-1, y)) &&
        (!level.isSolidAtPoint(x+16, y+16) || level.isSolidAtPoint(x+16, y)))
    {
      dir = (level.isSolidAtPoint(x-1, y) ? Dir.Right : Dir.Left);
      xVel = 0;
    } else if (dir == Dir.Left) {
      xVel = -1;
    } else {
      xVel = 1;
    }

    if (global.randOther(1, 100) == 1) {
      status = IDLE;
      counter = global.randOther(20, 50);
      xVel = 0;
    }
  }
  //if (isCollisionSolid()) y -= 2; // in the original
}


defaultproperties {
  objName = 'Alien';
  desc = "Alien";
  desc2 = "A hostile and intelligent extraterrestrial being.";

  setCollisionBounds(2, 6, 14, 16);
  xVel = 2.5;
  imageSpeed = 0.5;

  yVelLimit = 6; // YASM 1.7

  // stats
  hp = 1;
  invincible = 0;

  // status
  status = IDLE;

  dir = Dir.Right;

  bloodless = false;
  bloodLeft = 1;

  doBasicPhysics = false;
  countsAsKill = true;
  leavesBody = false; // default processing
  checkInsideBlock = true;
  allowWaterProcessing = true;

  depth = 60;
}
