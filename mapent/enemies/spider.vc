/**********************************************************************************
 * Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC
 * Copyright (c) 2010, Moloch
 * Copyright (c) 2018, Ketmar Dark
 *
 * This file is part of Spelunky.
 *
 * You can redistribute and/or modify Spelunky, including its source code, under
 * the terms of the Spelunky User License.
 *
 * Spelunky is distributed in the hope that it will be entertaining and useful,
 * but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.
 *
 * The Spelunky User License should be available in "Game Information", which
 * can be found in the Resource Explorer, or as an external file called COPYING.
 * If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>
 *
 **********************************************************************************/
class EnemySpiderHang['oSpiderHang'] : MapEnemy;

//int bounceCounter;
int alarm0Left;
int alarmDrowning;

name spriteHangName, spriteFlipName, spriteNormalName, spriteDrowningName;


void initNonHang () {
  setSprite(spriteNormalName);
  status = IDLE;
  setCollisionBounds(1, 5, 15, 16);
  xVel = 0;
  yVel = 0;
  yDelta = -0.4;
  imageSpeed = 0.4;
  flying = false;
}


override bool initialize () {
  if (!::initialize()) return false;
  setSprite(spriteHangName);
  return true;
}


override void onAnimationLooped () {
  if (spriteLName == spriteFlipName) setSprite(spriteNormalName);
}


// for green spider
void doBounce () {
  myGrav = 0.5;
  myGravNorm = 0.5;
  //if (global.bizarre) yVel = -1 * rand(3, 4) else yVel = -1 * rand(2, 5);
  //!!yVel = choose(-5, -7); // -rand(5, 7);
  yVel = (global.randOther(0, 1) ? -5 : -7);
  xVel = (level.player.ix < ix ? -2.5 : 2.5);
}


void onCollisionRight () {
  //xVel = 1; // in orig
  if (yVel > -2) xVel = 0; else if (xVel > 0.2) xVel -= 0.2;
}


void onCollisionLeft () {
  //xVel = -1; // in orig
  if (yVel > -2) xVel = 0; else if (xVel < -0.2) xVel += 0.2;
}


void onCollisionTop () {
  yVel = 1;
}


void onJumpFinishedNearPlayer () {
  xVel = 0;
  yVel = 0;
  status = RECOVER;
  alarm0Left = 30;
  /* // in orig
  yVel = -1 * rand(2, 5);
  if (oCharacter.x &lt; x+8) xVel = -2.5; else xVel = 2.5;
  if (rand(1, 4) == 1) { status = IDLE; xVel = 0; yVel = 0; }
  */
}


override void thinkFrame () {
  ::thinkFrame();
  if (!isInstanceAlive) return;


  if (status == DROWNED) {
    if (--alarmDrowning <= 0) {
      if (level.isWaterAtPoint(ix+8, iy+8)) { instanceRemove(); return; }
    }
    initNonHang();
  } else {
    alarmDrowning = 0;
  }


  if (status == HANG) {
    int x = ix, y = iy;

    if (level.isSolidAtPoint(x+8, y+4)) hp = 0;
    if (hp < 1) {
      spillBlood();
      //if (isRealLevel()) global.enemyKills[2] += 1;
      if (countsAsKill) level.addKill(objName);
      //global.spiders += 1;
      //global.kills += 1;
      instanceRemove();
      return;
    } else if (!level.isSolidAtPoint(x, y-16) ||
               (level.player.iy > y && abs(level.player.ix-(x+8)) < 8 && distanceToEntityCenter(level.player) < 90))
    {
      //spider = instance_create(x, y, oSpider);
      //spider.hp = hp;
      //instance_destroy();
      initNonHang();
      myGrav = 0.3; //0.2;
      myGravNorm = 0.3; //0.2;
      status = IDLE;
      setSprite(spriteFlipName);
    }
  }

  if (status != HANG) {
    if (alarm0Left > 0) {
      if (--alarm0Left == 0) {
        status = STATE_BOUNCE;
        if (isCollisionBottom(1)) doBounce();
      }
    }

    moveRel(xVel, yVel);
    int x = ix, y = iy;

    yVel = fmin(yVelLimit, yVel+myGrav);

    if (level.isSolidAtPoint(x+8, y+8)) hp = 0;

    if (hp < 1) {
      spillBlood();
      if (countsAsKill) level.addKill(objName);
      instanceRemove();
      return;
    }

    if (isCollisionRight(1)) onCollisionRight();
    if (isCollisionLeft(1)) onCollisionLeft();

    if (status == IDLE) {
      //if (global.bizarre) alarm[0] = rand(15, 20) else alarm[0] = rand(5, 20); // in orig
      alarm0Left = global.randOther(30, 60);
      status = RECOVER;
    } else if (status == RECOVER) {
      if (isCollisionBottom(1)) xVel = 0;
    } else if (status == STATE_BOUNCE && distanceToEntityCenter(level.player) < 80) { //90
      if (isCollisionBottom(1)) onJumpFinishedNearPlayer();
    } else if (status != DROWNED) {
      status = IDLE;
      //xVel = 0; // in orig
    }

    if (isCollisionTop(1)) onCollisionTop();
    //if (isCollisionLeft(1) or isCollisionRight(1)) xVel = -xVel; // in orig

    //if (isCollisionSolid()) y -= 2; // in orig
    if (spriteLName == spriteNormalName && yVel < 0) {
      imageFrame = 2;
      imageSpeed = 0;
    } else {
      imageSpeed = 0.4;
    }

    if (status != DROWNED && level.isWaterAtPoint(ix+8, iy+8)) {
      status = DROWNED;
      setSprite(spriteDrowningName);
      alarmDrowning = 30;
      xVel = 0;
      yVel = 0.2;
      level.MakeMapObject(ix+8, iy, 'oSplash');
      playSound('sndSplash');
    }
  }
}


defaultproperties {
  objName = 'Spider';
  desc = "Hanging Spider";
  desc2 = "With erratic movement patterns and sharp fangs, these arachnids are a menace to humans and animals alike.";
  status = /*IDLE*/HANG;
  hp = 1;
  yDelta = -0.4;

  flying = true; // don't fall

  imageSpeed = 0.4;

  setCollisionBounds(4, 0, 12, 12);

  doBasicPhysics = false;

  spriteHangName = 'sSpiderHang';
  spriteFlipName = 'sSpiderFlip';
  spriteNormalName = 'sSpider';
  spriteDrowningName = 'sSpiderDrowning';

  bloodless = false;
  allowWaterProcessing = false;
}


// ////////////////////////////////////////////////////////////////////////// //
class EnemySpider['oSpider'] : EnemySpiderHang;


override bool initialize () {
  if (!::initialize()) return false;
  initNonHang();
  return true;
}


// ////////////////////////////////////////////////////////////////////////// //
class EnemyGreenSpiderHang['oGreenSpiderHang'] : EnemySpiderHang;


override bool initialize () {
  if (!::initialize()) return false;
  writeln("Green Spider!");
  return true;
}


// for green spider
override void doBounce () {
  myGrav = 0.6;
  myGravNorm = 0.6;

  yVel = -global.randOther(3, 6); // -rand(5, 8);
  xVel = (level.player.ix < ix ? -global.randOther(1, 3) : global.randOther(1, 3));
}


override void onCollisionRight () {
  //xVel = 1; // in orig
  if (xVel > 0.2) xVel -= 0.2;
}


override void onCollisionLeft () {
  //xVel = -1; // in orig
  if (xVel < -0.2) xVel -= 0.2;
}


override void onCollisionTop () {
  yVel = 2;
}


override void onJumpFinishedNearPlayer () {
  yVel = -global.randOther(3, 5); //1, 3
  if (level.player.ix < ix+8) {
    xVel = (global.randOther(1, 4) == 1 ? 3 : -4);
  } else {
    xVel = (global.randOther(1, 4) == 1 ? -3 : 4);
  }
  if (global.randOther(1, 2) == 1) { status = IDLE; xVel = 0; yVel = 0; }
}


defaultproperties {
  objName = 'Green Spider';

  spriteHangName = 'sGreenSpiderHang';
  spriteFlipName = 'sGreenSpiderFlip';
  spriteNormalName = 'sGreenSpider';
  spriteDrowningName = 'sGreenSpiderDrowning';
}


// ////////////////////////////////////////////////////////////////////////// //
class EnemyGreenSpider['oGreenSpider'] : EnemyGreenSpiderHang;


override bool initialize () {
  if (!::initialize()) return false;
  initNonHang();
  return true;
}
