/**************************************************************************
 *    This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 3
 *  of the License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **************************************************************************/
// code by Ketmar Dark
// grid of entities
// note that entity can occupy more than one cell
class EntityGrid : Object;

enum CellSize = 16;


struct CellObject {
  MapEntity e;
  int tag; // arbitrary userdata, set to 0 in new items
  // last known grid position
  int gx0, gy0, gx1, gy1;
  // previous item in alloced list
  int prev;
  // next item in free/alloced list
  int next;
  bool objectIsOwned; // will be `delete`d on removing
}

struct Item {
  int cid; // index in `cobjs`
  // 0: end-of-list
  // otherwise either next item in cell, or next item in free list
  int next;
}


private array!CellObject cobjs;
private int freeHeadCObj;
private int firstInsertedCObj; // for `allObjects()`
private int lastInsertedCObj; // for `allObjects()`

// item #0 is never used
private array!Item items;
private int freeHead;

//WARNING! don't change
// grid size in cells
private int mWidth, mHeight;
// offset of the (0, 0) grid cell, in pixels
private int mXOfs, mYOfs;

// grid cells, width*height in total
// contains index in `items` or 0
private array!int grid;

private transient int lastUsedTag;

bool ownObjects; // should we own and `delete` inserted objects?

// this is used in various grid queries
transient private array!MapEntity collected;

transient int collUsed; // used items in `collected`


// in pixels
final int gridX0 { get mXOfs; }
final int gridY0 { get mYOfs; }
final int gridX1 { get { return mXOfs+mWidth*CellSize-1; } }
final int gridY1 { get { return mYOfs+mHeight*CellSize-1; } }
final int gridWidth { get { return mWidth*CellSize; } }
final int gridHeight { get { return mHeight*CellSize; } }


// ////////////////////////////////////////////////////////////////////////// //
final void setup (int x0, int y0, int awidth, int aheight) {
  awidth = max(1, awidth);
  aheight = max(1, aheight);
  int xcells = max(1, (awidth+CellSize-1)/CellSize);
  int ycells = max(1, (aheight+CellSize-1)/CellSize);
  // setup offsets
  mXOfs = x0;
  mYOfs = y0;
  // setup dimensions
  mWidth = xcells;
  mHeight = ycells;
  // setup items and free list
  items.length = 32;
  cobjs.length = 32;
  grid.setSize(xcells, ycells);
  removeAllObjects();
}


final void clear () {
  if (collUsed) FatalError("EntityGrid::clear(): query in progress");
  if (lastSafeIter) FatalError("EntityGrid::clear(): safe query in progress");
  cobjs.length = 0;
  freeHeadCObj = 0;
  items.length = 0;
  freeHead = 0;
  mWidth = 0;
  mHeight = 0;
  mXOfs = 0;
  mYOfs = 0;
  grid.length = 0;
  lastUsedTag = 0;
  collected.length = 0;
}


final void removeAllObjects (optional bool doRemove) {
  if (collUsed) FatalError("EntityGrid::removeAllObjects(): query in progress");
  if (lastSafeIter) FatalError("EntityGrid::clear(): safe query in progress");
  lastUsedTag = 0;
  // clear grid
  foreach (ref auto oid; grid) oid = 0;
  // clear items
  items.length = 32;
  foreach (int f, ref auto item; items) item.next = f+1;
  items[0].next = 0; // as sentinel
  items[$-1].next = 0;
  freeHead = 1;
  // clear cobjects
  for (int f = firstInsertedCObj; f; f = cobjs[f].next) {
    if (doRemove || cobjs[f].objectIsOwned) delete cobjs[f].e; else cobjs[f].e = none;
  }
  cobjs.length = 32;
  foreach (int f, ref auto cobj; cobjs) {
    cobj.e = none;
    cobj.prev = -1;
    cobj.next = f+1;
    cobj.tag = 0;
    cobj.objectIsOwned = false;
  }
  cobjs[0].next = 0; // as sentinel
  cobjs[$-1].next = 0;
  freeHeadCObj = 1;
  firstInsertedCObj = 0;
  lastInsertedCObj = 0;
  collected.clear(); // just in case
}


private final void clearTags () {
  for (int oid = firstInsertedCObj; oid; oid = cobjs[oid].next) {
    cobjs[oid].tag = 0;
  }
}


private final int nextTag () {
  if (lastUsedTag >= 0x3fff_ffff && collUsed == 0) {
    lastUsedTag = 0;
    clearTags();
  }
  if (lastUsedTag == 0x7fff_ffff) FatalError("EntityGrid: tag overflow");
  return ++lastUsedTag;
}


// ////////////////////////////////////////////////////////////////////////// //
private final int allocCObj () {
  int res = freeHeadCObj;
  if (res) {
    freeHeadCObj = cobjs[res].next;
  } else {
    res = cobjs.length;
    int newlen = (res > 3 ? res+res/2 : 8);
    cobjs.length = newlen;
    foreach (int f; res+1..newlen) { cobjs[f].prev = -1; cobjs[f].next = f+1; }
    cobjs[$-1].next = 0;
    freeHeadCObj = res+1;
  }
  // other fields will be set by caller
  auto cobj = &cobjs[res];
  if (lastInsertedCObj) cobjs[lastInsertedCObj].next = res; else firstInsertedCObj = res;
  cobj.tag = 0;
  cobj.prev = lastInsertedCObj;
  cobj.next = 0;
  cobj.objectIsOwned = ownObjects;
  lastInsertedCObj = res;
  return res;
}


private final void freeCObj (int idx) {
  if (idx < 1 || idx >= cobjs.length) FatalError("EntityGrid::freeCObj: invalid item index!");
  auto cobj = &cobjs[idx];
  if (cobj.prev == -1) FatalError("EntityGrid::freeCObj: double free!");
  // fix safe iterators
  for (AllObjSafeIterData *it = lastSafeIter; it; it = it.next) {
    if (it.cid == idx) {
      // move to the next one
      it.cid = cobjs[it.cid].next;
    }
  }
  // remove us from the alloced list
  if (cobj.prev) cobjs[cobj.prev].next = cobj.next;
  if (cobj.next) cobjs[cobj.next].prev = cobj.prev;
  // fix alloced list (and do some sanity checks)
  if (idx == lastInsertedCObj) {
    if (cobj.next != 0) FatalError("EntityGrid::freeCObj: invalid alloced list!");
    lastInsertedCObj = cobj.prev;
  }
  if (idx == firstInsertedCObj) {
    if (cobj.prev != 0) FatalError("EntityGrid::freeCObj: invalid alloced list!");
    firstInsertedCObj = cobj.next;
  }
  MapEntity e = cobj.e;
  if (cobj.objectIsOwned) {
    delete e;
  } else if (e) {
    e.gridId = 0;
    e.grid = none;
  }
  cobj.e = none;
  cobj.prev = -1;
  cobj.next = freeHeadCObj;
  cobj.objectIsOwned = false;
  freeHeadCObj = idx;
}


private final int allocItem () {
  int res = freeHead;
  if (res) {
    freeHead = items[res].next;
  } else {
    res = items.length;
    int newlen = (res > 3 ? res+res/2 : 8);
    items.length = newlen;
    foreach (int f; res+1..newlen) items[f].next = f+1;
    items[$-1].next = 0;
    freeHead = res+1;
  }
  // other fields will be set by caller
  return res;
}


private final void freeItem (int idx) {
  if (idx < 1 || idx >= items.length) FatalError("EntityGrid::freeItem: invalid item index!");
  if (items[idx].cid == -1) FatalError("EntityGrid::freeItem: double free!");
  items[idx].cid = -1;
  items[idx].next = freeHead;
  freeHead = idx;
}


// ////////////////////////////////////////////////////////////////////////// //
private final void insertIntoCell (int cid, int gx, int gy) {
  int iid = allocItem();
  Item *it = &items[iid];
  it.cid = cid;
  it.next = grid[gx, gy];
  grid[gx, gy] = iid;
}


private final void removeFromCell (int cid, int gx, int gy) {
  // find item
  int iid = grid[gx, gy], iprev = 0;
  while (iid) {
    if (items[iid].cid == cid) break;
    iprev = iid;
    iid = items[iid].next;
  }
  if (iid) {
    // i found her!
    int next = items[iid].next;
    if (iprev) items[iprev].next = next; else grid[gx, gy] = next;
    freeItem(iid);
  }
}


// `cobjs[cid]` should be fully initialized
private final void insertInternal (int cid) {
  auto cobj = &cobjs[cid];
  int gx0 = cobj.gx0, gx1 = cobj.gx1;
  foreach (int cy; cobj.gy0..cobj.gy1) {
    foreach (int cx; gx0..gx1) {
      insertIntoCell(cid, cx, cy);
    }
  }
}


// this won't free `cid`
private final void removeInternal (int cid) {
  auto cobj = &cobjs[cid];
  int gx0 = cobj.gx0, gx1 = cobj.gx1;
  foreach (int cy; cobj.gy0..cobj.gy1) {
    foreach (int cx; gx0..gx1) {
      removeFromCell(cid, cx, cy);
    }
  }
}


// returns `true` if moved
private final bool moveInternal (int cid, int nx0, int ny0, int nx1, int ny1) {
  auto cobj = &cobjs[cid];
  int gx0 = cobj.gx0, gy0 = cobj.gy0;
  int gx1 = cobj.gx1, gy1 = cobj.gy1;
  if (gx0 != nx0 || gy0 != ny0 || gx1 != nx1 || gy1 != ny1) {
    foreach (int gy; min(gy0, ny0)..max(gy1, ny1)) {
      foreach (int gx; min(gx0, nx0)..max(gx1, nx1)) {
        // are we inside the old rect?
        if (gx >= gx0 && gy >= gy0 && gx < gx1 && gy < gy1) {
          // old rect: inside
          // if not inside new, remove
          if (gx < nx0 || gy < ny0 || gx >= nx1 || gy >= ny1) {
            removeFromCell(cid, gx, gy);
          }
        } else {
          // old rec: outside
          // if inside new, insert
          if (gx >= nx0 && gy >= ny0 && gx < nx1 && gy < ny1) {
            insertIntoCell(cid, gx, gy);
          }
        }
      }
    }
    //removeInternal(cid);
    cobj.gx0 = nx0; cobj.gy0 = ny0;
    cobj.gx1 = nx1; cobj.gy1 = ny1;
    //insertInternal(cid);
    return true;
  }
  return false;
}


// `false`: out of grid/too thin/etc
// returned coords are always valid
// also, `gx1` and `gy1` are exclusive
final bool calcObjGridPos (MapEntity e, out int gx0, out int gy0, out int gx1, out int gy1) {
  if (!e) { gx0 = 0; gy0 = 0; gx1 = 0; gy1 = 0; return false; } // no object
  int ew = max(1, e.width), eh = max(1, e.height);
  //if (ew < 1 || eh < 1) { gx0 = 0; gy0 = 0; gx1 = 0; gy1 = 0; return false; }
  int ex0 = e.x0-mXOfs, ey0 = e.y0-mYOfs;
  int ex1 = ex0+ew-1, ey1 = ey0+eh-1;
  // check if it fits in grid at all
  if (ex1 < 0 || ey1 < 0) { gx0 = 0; gy0 = 0; gx1 = 0; gy1 = 0; return false; } // out of grid
  int tgw = mWidth, tgh = mHeight;
  if (ex0 >= tgw*CellSize || ey0 >= tgh*CellSize) { gx0 = 0; gy0 = 0; gx1 = 0; gy1 = 0; return false; } // out of grid
  // calc grid coords
  ex0 /= CellSize;
  ey0 /= CellSize;
  ex1 /= CellSize;
  ey1 /= CellSize;
  gx0 = clamp(ex0, 0, tgw-1);
  gy0 = clamp(ey0, 0, tgh-1);
  gx1 = clamp(ex1+1, 0, tgw);
  gy1 = clamp(ey1+1, 0, tgh);
  return (gx1 > gx0 && gy1 > gy0);
}


// ////////////////////////////////////////////////////////////////////////// //
// returns 0 if item is not put in grid, or cid
final int insert (MapEntity e) {
  if (!e) return 0;
  if (e.gridId) {
    if (e.grid != self) FatalError("cannot instert entity in two grids");
    if (!update(e.gridId)) { e.gridId = 0; e.grid = none; }
    return e.gridId;
  }
  int gx0, gy0, gx1, gy1;
  bool doInsert = calcObjGridPos(e, out gx0, out gy0, out gx1, out gy1);
  // allocate cobj
  int cid = allocCObj();
  auto cobj = &cobjs[cid];
  e.gridId = cid;
  e.grid = self;
  cobj.e = e;
  cobj.tag = 0;
  cobj.gx0 = gx0; cobj.gy0 = gy0;
  cobj.gx1 = gx1; cobj.gy1 = gy1;
  if (doInsert) insertInternal(cid);
  return cid;
}


// returns `true` if item was in the grid, and now removed
final bool remove (int cid) {
  if (cid < 1 || cid >= cobjs.length) return false;
  if (cobjs[cid].prev == -1) return false;
  removeInternal(cid);
  freeCObj(cid);
  return true;
}


// call this when object changed its position
// it is harmless to call it even if position wasn't changed
// returns `false` if item should be removed from grid
// optionally sets `wasMoved` flag
final bool update (int cid, optional out bool wasMoved/*, optional bool markAsDead*/) {
  if (specified_wasMoved) wasMoved = false;
  if (cid < 1 || cid >= cobjs.length) return false;
  auto cobj = &cobjs[cid];
  if (cobj.prev == -1) return false;
  MapEntity e = cobj.e;
  if (!e) return false;
  int gx0, gy0, gx1, gy1;
  if (!calcObjGridPos(e, out gx0, out gy0, out gx1, out gy1)) {
    //if (markAsDead && !e.isZeroDimsValid && (e.width < 1 || e.height < 1)) e.instanceRemove();
    removeInternal(cid);
    cobj.gx0 = 0; cobj.gy0 = 0;
    cobj.gx1 = 0; cobj.gy1 = 0;
    return false;
  }
  wasMoved = moveInternal(cid, gx0, gy0, gx1, gy1);
  return true;
}


// ////////////////////////////////////////////////////////////////////////// //
// ok, it is not a real spawner, i just wanted to fake its return type
final spawner MapEntity getObject (class!MapEntity cc, int cid) {
  if (cid < 1 || cid >= cobjs.length) return none;
  auto cobj = &cobjs[cid];
  if (cobj.prev == -1) return none;
  return cc(cobj.e);
}

final bool getOwned (int cid) {
  if (cid < 1 || cid >= cobjs.length) return false;
  auto cobj = &cobjs[cid];
  return cobj.objectIsOwned;
}

final void setOwned (int cid, bool v) {
  if (cid < 1 || cid >= cobjs.length) return;
  auto cobj = &cobjs[cid];
  if (cobj.prev != -1) cobj.objectIsOwned = v;
}


// ////////////////////////////////////////////////////////////////////////// //
// ok, it is not a real spawner, i just wanted to fake its return type
final spawner MapEntity getObjectByItem (class!MapEntity cc, int id) {
  if (id < 1 || id >= items.length) return none;
  MapEntity e = cobjs[items[id].cid].e;
  if (!e || !e.isInstanceAlive) return none;
  return cc(e);
}


// returns first item in cell, optionally tags it too (and skips the given tag)
// returns 0 if there are no items
final int getFirstItemInCell (int cx, int cy, optional int tag) {
  //writeln("specified_tag=", specified_tag, "; tag=", tag);
  if (cx < 0 || cy < 0 || cx >= mWidth || cy >= mHeight) return 0;
  int oid = grid[cx, cy];
  while (oid) {
    // check tag
    if (specified_tag) {
      CellObject *co = &cobjs[items[oid].cid];
      if (co.tag == tag) {
        //writeln("***TAG SKIP (0)");
        oid = items[oid].next;
        continue;
      }
      co.tag = tag; // mark it
    }
    return oid;
  }
  return 0;
}


// returns first item in cell, optionally tags it too (and skips the given tag)
// returns 0 if there are no items
final int getFirstItem (int x, int y, optional int tag) {
  return getFirstItemInCell((x-mXOfs)/CellSize, (y-mYOfs)/CellSize, tag!optional);
}


// returns next item in list, optionally tags it too (and skips the given tag)
// returns 0 if there are no more items
final int getNextItem (int oid, optional int tag) {
  oid = items[oid].next; // 0 contains sentinel anyway
  while (oid) {
    // check tag
    if (specified_tag) {
      CellObject *co = &cobjs[items[oid].cid];
      if (co.tag == tag) {
        //writeln("***TAG SKIP (1)");
        oid = items[oid].next;
        continue;
      }
      co.tag = tag; // mark it
    }
    return oid;
  }
  return 0;
}


// ////////////////////////////////////////////////////////////////////////// //
// low-level iterator builder
// `GridIteratorData` contents doesn't matter
private final void collectObjectsInRect (out GridIteratorData idata, int ax, int ay, int aw, int ah, bool precise, class!MapEntity castClass,
                                         scope bool delegate (MapEntity e, int x0, int y0, int width, int height) cdg)
{
  int cused = collUsed;
  idata.collIdx = cused;
  idata.collStart = cused;
  idata.collEnd = cused;
  if (aw < 1 || ah < 1) return;
  int grX = ax-mXOfs, grY = ay-mYOfs;
  if (grX+aw <= 0 || grY+ah <= 0 || grX >= mWidth*CellSize || grY >= mHeight*CellSize) return;
  int cx0 = max(0, grX/CellSize);
  int cy0 = max(0, grY/CellSize);
  int cx1 = min(mWidth, (grX+aw-1)/CellSize+1);
  int cy1 = min(mHeight, (grY+ah-1)/CellSize+1);
  auto tag = nextTag(); // the tag will be used to reject duplicate objects
  //writeln("(", ax, ",", ay, ")-(", ax+aw-1, ",", ay+ah-1, "): (", cx0, ",", cy0, ")-(", cx1, ",", cy1, ")");
  foreach (int cy; cy0..cy1) {
    foreach (int cx; cx0..cx1) {
      int oid = getFirstItemInCell(cx, cy, tag);
      while (oid) {
        MapEntity e = getObjectByItem(castClass, oid); // `e` is guaranteed to be valid or `none`
        if (e isa castClass) {
          if (!precise || cdg(e, ax, ay, aw, ah)) collected[cused++] = e;
        }
        oid = getNextItem(oid, tag);
      }
    }
  }
  idata.castClass = castClass;
  idata.collEnd = cused;
  collUsed = cused;
}


// ////////////////////////////////////////////////////////////////////////// //
// high-level iterators
// you can add/remove objects while iterating, it is harmless
// newly added objects may, or may not participate in current frame processing
// removed objects will be returned unless they are marked dead with `obj.instanceRemove()`

struct GridIteratorData {
  // positions in `collected`
  int collIdx; // current index
  int collStart; // first item for this iterator
  int collEnd; // last (exclusive) item for this iterator
  class!MapEntity castClass;
}


// ////////////////////////////////////////////////////////////////////////// //
// pixel checker
final private bool pixelCheckerDG (MapEntity e, int x0, int y0, int width, int height) {
  return e.isPointCollision(x0, y0);
}


// collect objects at a given pixel
// if `precise` is `false`, collect objects with cell granularity
final bool inCellPix_opIterInit (ref GridIteratorData it, int x, int y,
                                 optional bool precise, optional class!MapEntity castClass)
{
  if (!specified_precise) precise = true;
  if (!castClass) castClass = MapEntity;
  collectObjectsInRect(it, x, y, 1, 1, precise, castClass, &pixelCheckerDG);
  //writeln("*** ICP_INIT: start=", it.collStart, "; end=", it.collEnd, "; collUsed=", collUsed, "; valid=", (it.collEnd > it.collStart));
  return (it.collEnd > it.collStart);
}


final bool inCellPix_opIterNext (ref GridIteratorData it, out MapEntity ge) {
  while (it.collIdx < it.collEnd) {
    MapEntity e = collected[it.collIdx++];
    // this object may be marked as dead or `delete`d, so check for this
    if (e && e.isInstanceAlive) {
      ge = e;
      return true;
    }
  }
  return false; // this iterator is no more
}


final void inCellPix_opIterDone (ref GridIteratorData it) {
  //writeln("*** ICP_DONE: start=", it.collStart, "; end=", it.collEnd, "; collUsed=", collUsed, "; new collUsed=", it.collStart);
  if (it.collEnd > it.collStart) {
    if (collUsed != it.collEnd) FatalError("EntityGrid::inCellPix: unbalanced iterators");
    collUsed = it.collStart;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// rect checker
final private bool rectCheckerDG (MapEntity e, int x0, int y0, int width, int height) {
  return e.isRectCollision(x0, y0, width, height);
}


// collect objects at a given pixel
// if `precise` is `false`, collect objects with cell granularity
final bool inRectPix_opIterInit (ref GridIteratorData it, int x, int y, int width, int height,
                                 optional bool precise, optional class!MapEntity castClass)
{
  if (!specified_precise) precise = true;
  if (!castClass) castClass = MapEntity;
  collectObjectsInRect(it, x, y, width, height, precise, castClass, &rectCheckerDG);
  //writeln("*** IRP_INIT: start=", it.collStart, "; end=", it.collEnd, "; collUsed=", collUsed, "; valid=", (it.collEnd > it.collStart));
  return (it.collEnd > it.collStart);
}


final bool inRectPix_opIterNext (ref GridIteratorData it, out MapEntity ge) {
  while (it.collIdx < it.collEnd) {
    MapEntity e = collected[it.collIdx++];
    // this object may be marked as dead or `delete`d, so check for this
    if (e && e.isInstanceAlive) {
      ge = e;
      return true;
    }
  }
  return false; // this iterator is no more
}


final void inRectPix_opIterDone (ref GridIteratorData it) {
  //writeln("*** IRP_DONE: start=", it.collStart, "; end=", it.collEnd, "; collUsed=", collUsed, "; new collUsed=", it.collStart);
  if (it.collEnd > it.collStart) {
    if (collUsed != it.collEnd) FatalError("EntityGrid::inRectPix: unbalanced iterators");
    collUsed = it.collStart;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// 0: no objects
final int getFirstObjectCID () { return firstInsertedCObj; }

// 0: no more objects
final int getNextObjectCID (int cid) {
  if (cid < 1 || cid >= cobjs.length || cobjs[cid].prev == -1) return 0;
  int n = cobjs[cid].next;
  /*
  if (removeThis) {
    removeInternal(cid);
    freeCObj(cid);
  }
  */
  return n;
}


// 0: no objects
final int getLastObjectCID () { return lastInsertedCObj; }

// 0: no more objects
final int getPrevObjectCID (int cid) {
  if (cid < 1 || cid >= cobjs.length || cobjs[cid].prev == -1) return 0;
  int n = cobjs[cid].prev;
  /*
  if (removeThis) {
    removeInternal(cid);
    freeCObj(cid);
  }
  */
  return n;
}


// ////////////////////////////////////////////////////////////////////////// //
// creates "safe" iterator: you can insert/remove items while iterating

transient private AllObjSafeIterData *lastSafeIter;

struct AllObjSafeIterData {
  AllObjSafeIterData *next;
  int cid;
  class!MapEntity castClass;
}

final bool allObjectsSafe_opIterInit (ref AllObjSafeIterData idata, optional class!MapEntity castClass) {
  if (!castClass) castClass = MapEntity;
  int cid = firstInsertedCObj;
  while (cid) {
    MapEntity e = cobjs[cid].e;
    if (e isa castClass && e.isInstanceAlive) {
      idata.next = lastSafeIter;
      lastSafeIter = &idata;
      idata.cid = cid;
      idata.castClass = castClass;
      return true;
    }
    cid = cobjs[cid].next;
  }
  idata.next = nullptr;
  idata.cid = -666; // "unused" flag
  return false;
}

final bool allObjectsSafe_opIterNext (ref AllObjSafeIterData idata, out MapEntity ge) {
  int cid = idata.cid;
  auto cc = idata.castClass;
  while (cid > 0) {
    MapEntity e = cobjs[cid].e;
    cid = cobjs[cid].next;
    if (e isa cc && e.isInstanceAlive) {
      idata.cid = cid;
      ge = e;
      return true;
    }
  }
  idata.cid = 0; // mark "complete" (just in case)
  return false;
}

final void allObjectsSafe_opIterDone (ref AllObjSafeIterData idata) {
  if (idata.cid != -666) {
    // remove iterator from list
    if (lastSafeIter != &idata) FatalError("EntityGrid::allObjectsSafe: unbalanced iterators");
    lastSafeIter = idata.next;
    idata.next = nullptr;
    idata.cid = -666;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
struct AllObjIterData {
  int cid;
  class!MapEntity castClass;
}

final bool allObjects_opIterInit (ref AllObjIterData idata, optional class!MapEntity castClass) {
  if (!castClass) castClass = MapEntity;
  int cid = firstInsertedCObj;
  while (cid) {
    MapEntity e = cobjs[cid].e;
    if (e isa castClass && e.isInstanceAlive) {
      idata.cid = cid;
      idata.castClass = castClass;
      return true;
    }
    cid = cobjs[cid].next;
  }
  idata.cid = -666;
  return false;
}

final bool allObjects_opIterNext (ref AllObjIterData idata, out MapEntity ge) {
  int cid = idata.cid;
  auto cc = idata.castClass;
  while (cid > 0) {
    MapEntity e = cobjs[cid].e;
    cid = cobjs[cid].next;
    if (e isa cc && e.isInstanceAlive) {
      idata.cid = cid;
      ge = e;
      return true;
    }
  }
  idata.cid = 0; // mark "complete" (just in case)
  return false;
}


// ////////////////////////////////////////////////////////////////////////// //
// hack for make some iterations (especially frame thinker) slightly faster
final void collectAllActiveObjects (ref array!MapEntity aoarr) {
  int cid = firstInsertedCObj;
  while (cid) {
    MapEntity e = cobjs[cid].e;
    if (e && e.active && e.isInstanceAlive) aoarr[$] = e;
    cid = cobjs[cid].next;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
/+
final bool allObjectsBackwards_opIterInit (ref int cid) {
  cid = lastInsertedCObj;
  while (cid && !cobjs[cid].e) cid = cobjs[cid].prev;
  return (cid != 0);
}

final bool allObjectsBackwards_opIterNext (ref int cid, out MapEntity ge) {
  while (cid) {
    MapEntity e = cobjs[cid].e;
    cid = cobjs[cid].prev;
    if (e && e.isInstanceAlive) { ge = e; return true; }
  }
  return false;
}
+/


// ////////////////////////////////////////////////////////////////////////// //
// some debug shit
final int countObjects () {
  int objCount = 0;
  for (int f = firstInsertedCObj; f; f = cobjs[f].next) ++objCount;
  return objCount;
}


final void dumpStats () {
  // calculate max object in cell, number of free cells, and number of used items
  int freeCells = 0, maxInCell = 0, itemCount = 0;
  foreach (int oid; grid) {
    if (!oid) {
      ++freeCells;
    } else {
      int xc = 0;
      while (oid) {
        ++xc;
        oid = items[oid].next;
      }
      itemCount += xc;
      maxInCell = max(maxInCell, xc);
    }
  }
  int objCount = countObjects();
  writeln("=========== GRID STATS ===========");
  writeln("bounds: (", mXOfs, ",", mYOfs, ")-(", mXOfs+mWidth*CellSize-1, ",", mYOfs+mHeight*CellSize-1, ")");
  writeln("cells: ", grid.length, " (", grid.length1, "x", grid.length2, ")");
  writeln("objects: ", objCount, " (out of ", cobjs.length, " slots)");
  writeln("used items: ", itemCount, " out of ", items.length);
  writeln("max items in cell: ", maxInCell);
  writeln("used cells: ", grid.length-freeCells, " out of ", grid.length);
  writeln("----------------------------------");
}


// ////////////////////////////////////////////////////////////////////////// //
/+
struct LineWalker {
  int wx0, wy0, wx1, wy1; // window coordinates
  int stx, sty; // "steps" for x and y axes
  int stleft; // "steps left"
  int err, errinc, errmax;
  int xd, yd; // current coord
  int horiz; // bool

  alias x = xd;
  alias y = yd;
}


// call `lwSetup()` after this
static final void lwCreate (ref LineWalker lw, int minx, int miny, int maxx, int maxy) {
  // clip rectange
  lw.wx0 = minx;
  lw.wy0 = miny;
  lw.wx1 = maxx;
  lw.wy1 = maxy;
}


static final void lwSetClip (ref LineWalker lw, int minx, int miny, int maxx, int maxy) {
  // clip rectange
  lw.wx0 = minx;
  lw.wy0 = miny;
  lw.wx1 = maxx;
  lw.wy1 = maxy;
}


// this will use `w[xy][01]` to clip coords
// return `false` if the whole line was clipped away
// on `true`, you should process first point, and go on
static final bool lwSetup (ref LineWalker lw, int x0, int y0, int x1, int y1) {
  if (lw.wx1 < lw.wx0 || lw.wy1 < lw.wy0) { lw.stleft = 0; lw.xd = x0; lw.yd = y0; return false; }

  bool res;
  if (x0 >= lw.wx0 && y0 >= lw.wy0 && x0 <= lw.wx1 && y0 <= lw.wy1 &&
      x1 >= lw.wx0 && y1 >= lw.wy0 && x1 <= lw.wx1 && y1 <= lw.wy1)
  {
    res = true;
  } else {
    float sx0 = x0, sy0 = y0;
    float sx1 = x1, sy1 = y1;
    res = Geom.clipLine(sx0, sy0, sx1, sy1, lw.wx0, lw.wy0, lw.wx1, lw.wy1);
    if (!res) { lw.stleft = 0; lw.xd = x0; lw.yd = y0; return false; }
    x0 = trunci(sx0); y0 = trunci(sy0);
    x1 = trunci(sx1); y1 = trunci(sy1);
  }

  // check for ortho lines
  if (y0 == y1) {
    // horizontal
    lw.horiz = true;
    lw.stleft = abs(x1-x0)+1;
    lw.stx = (x0 < x1 ? 1 : -1);
    lw.sty = 0;
    lw.errinc = 0;
    lw.errmax = 10; // anything that is greater than zero
  } else if (x0 == x1) {
    // vertical
    lw.horiz = false;
    lw.stleft = abs(y1-y0)+1;
    lw.stx = 0;
    lw.sty = (y0 < y1 ? 1 : -1);
    lw.errinc = 0;
    lw.errmax = 10; // anything that is greater than zero
  } else {
    // diagonal
    if (abs(x1-x0) >= abs(y1-y0)) {
      // horizontal
      lw.horiz = true;
      lw.stleft = abs(x1-x0)+1;
      lw.errinc = abs(y1-y0)+1;
    } else {
      // vertical
      lw.horiz = false;
      lw.stleft = abs(y1-y0)+1;
      lw.errinc = abs(x1-x0)+1;
    }
    lw.stx = (x0 < x1 ? 1 : -1);
    lw.sty = (y0 < y1 ? 1 : -1);
    lw.errmax = lw.stleft;
  }
  lw.xd = x0;
  lw.yd = y0;
  lw.err = -lw.errmax;
  return res;
}


// call this *after* doing a step
// WARNING! if you will do a step when this returns `true`, you will fall into limbo
static final bool lwIsDone (ref LineWalker lw) {
  return (lw.stleft <= 0);
}


// as you will prolly call `done()` after doing a step anyway, this will do it for you
// move to next point, return `true` when the line is complete (i.e. you should stop)
static final bool step (ref LineWalker lw) {
  if (lw.horiz) {
    lw.xd += lw.stx;
    lw.err += lw.errinc;
    if (lw.err >= 0) { lw.err -= lw.errmax; lw.yd += lw.sty; }
  } else {
    lw.yd += lw.sty;
    lw.err += lw.errinc;
    if (lw.err >= 0) { lw.err -= lw.errmax; lw.xd += lw.stx; }
  }
  --lw.stleft;
  return (lw.stleft <= 0);
}


// move to next tile; return `true` if the line is complete (and walker state is undefined then)
static final bool stepToNextTile (ref LineWalker lw) {
  if (lw.stleft < 2) return true; // max one pixel left, nothing to do

  int ex, ey;

  // strictly horizontal?
  if (lw.sty == 0) {
    // only xd
    if (lw.stx < 0) {
      // xd: to left edge
      ex = (lw.xd&(~(CellSize-1)))-1;
      lw.stleft -= lw.xd-ex;
    } else {
      // xd: to right edge
      ex = (lw.xd|(CellSize-1))+1;
      lw.stleft -= ex-lw.xd;
    }
    lw.xd = ex;
    return (lw.stleft <= 0);
  }

  // strictly vertical?
  if (lw.stx == 0) {
    // only xd
    if (lw.sty < 0) {
      // yd: to top edge
      ey = (lw.yd&(~(CellSize-1)))-1;
      lw.stleft -= lw.yd-ey;
    } else {
      // yd: to bottom edge
      ey = (lw.yd|(CellSize-1))+1;
      lw.stleft -= ey-lw.yd;
    }
    lw.yd = ey;
    return (lw.stleft <= 0);
  }

  // diagonal
  int xwalk, ywalk;

  // calculate xwalk
  if (lw.stx < 0) {
    ex = (lw.xd&(~(CellSize-1)))-1;
    xwalk = lw.xd-ex;
  } else {
    ex = (lw.xd|(CellSize-1))+1;
    xwalk = ex-lw.xd;
  }

  // calculate ywalk
  if (lw.sty < 0) {
    ey = (lw.yd&(~(CellSize-1)))-1;
    ywalk = lw.yd-ey;
  } else {
    ey = (lw.yd|(CellSize-1))+1;
    ywalk = ey-lw.yd;
  }

  int wklen = (xwalk <= ywalk ? xwalk : ywalk);
  while (true) {
    // in which dir we want to walk?
    lw.stleft -= wklen;
    if (lw.stleft <= 0) return true;
    if (lw.horiz) {
      lw.xd += wklen*lw.stx;
      for (int f = wklen; f > 0; --f) {
        lw.err += lw.errinc;
        if (lw.err >= 0) { lw.err -= lw.errmax; lw.yd += lw.sty; }
      }
    } else {
      lw.yd += wklen*lw.sty;
      for (int f = wklen; f > 0; --f) {
        lw.err += lw.errinc;
        if (lw.err >= 0) { lw.err -= lw.errmax; lw.xd += lw.stx; }
      }
    }
    // check for walk completion
    if (lw.xd == ex || lw.yd == ey) break;
    wklen = 1;
  }

  return false;
}
+/


// ////////////////////////////////////////////////////////////////////////// //
// unfinished line walker from GameLevel
//FIXME: optimize this with clipping first
//TODO: moveable tiles
/+
final MapTile checkTilesAtLine (int ax0, int ay0, int ax1, int ay1, optional scope bool delegate (MapTile dg) dg) {
  // do it faster if we can

  // strict vertical check?
  if (ax0 == ax1 && ay0 <= ay1) return checkTilesInRect(ax0, ay0, 1, ay1-ay0+1, dg!optional);
  // strict horizontal check?
  if (ay0 == ay1 && ax0 <= ax1) return checkTilesInRect(ax0, ay0, ax1-ax0+1, 1, dg!optional);

  float x0 = float(ax0)/16.0, y0 = float(ay0)/16.0, x1 = float(ax1)/16.0, y1 = float(ay1)/16.0;

  // fix delegate
  if (!dg) dg = &cbCollisionAnySolid;

  // get starting and enging tile
  int tileSX = trunci(x0), tileSY = trunci(y0);
  int tileEX = trunci(x1), tileEY = trunci(y1);

  // first hit is always landed
  if (tileSX >= 0 && tileSY >= 0 && tileSX < TilesWidth && tileSY < TilesHeight) {
    MapTile t = tiles[tileSX, tileSY];
    if (t && t.visible && Geom.lineAABBIntersects(ax0, ay0, ax1, ay1, tileSX*16, tileSY*16, 16, 16) && dg(t)) return t;
  }

  // if starting and ending tile is the same, we don't need to do anything more
  if (tileSX == tileEX && tileSY == tileEY) return none;

  // calculate ray direction
  TVec dv = (vector(x1, y1)-vector(x0, y0)).normalise2d;

  // length of ray from one x or y-side to next x or y-side
  float deltaDistX = (fabs(dv.x) > 0.0001 ? fabs(1.0/dv.x) : 0.0);
  float deltaDistY = (fabs(dv.y) > 0.0001 ? fabs(1.0/dv.y) : 0.0);

  // calculate step and initial sideDists

  float sideDistX; // length of ray from current position to next x-side
  int stepX; // what direction to step in x (either +1 or -1)
  if (dv.x < 0) {
    stepX = -1;
    sideDistX = (x0-tileSX)*deltaDistX;
  } else {
    stepX = 1;
    sideDistX = (tileSX+1.0-x0)*deltaDistX;
  }

  float sideDistY; // length of ray from current position to next y-side
  int stepY; // what direction to step in y (either +1 or -1)
  if (dv.y < 0) {
    stepY = -1;
    sideDistY = (y0-tileSY)*deltaDistY;
  } else {
    stepY = 1;
    sideDistY = (tileSY+1.0-y0)*deltaDistY;
  }

  // perform DDA
  //int side; // was a NS or a EW wall hit?
  for (;;) {
    // jump to next map square, either in x-direction, or in y-direction
    if (sideDistX < sideDistY) {
      sideDistX += deltaDistX;
      tileSX += stepX;
      //side = 0;
    } else {
      sideDistY += deltaDistY;
      tileSY += stepY;
      //side = 1;
    }
    // check tile
    if (tileSX >= 0 && tileSY >= 0 && tileSX < TilesWidth && tileSY < TilesHeight) {
      MapTile t = tiles[tileSX, tileSY];
      if (t && t.visible && Geom.lineAABBIntersects(ax0, ay0, ax1, ay1, tileSX*16, tileSY*16, 16, 16) && dg(t)) return t;
    }
    // did we arrived at the destination?
    if (tileSX == tileEX && tileSY == tileEY) break;
  }

  return none;
}
+/
