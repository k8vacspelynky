/**********************************************************************************
 * Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC
 * Copyright (c) 2010, Moloch
 * Copyright (c) 2018, Ketmar Dark
 *
 * This file is part of Spelunky.
 *
 * You can redistribute and/or modify Spelunky, including its source code, under
 * the terms of the Spelunky User License.
 *
 * Spelunky is distributed in the hope that it will be entertaining and useful,
 * but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.
 *
 * The Spelunky User License should be available in "Game Information", which
 * can be found in the Resource Explorer, or as an external file called COPYING.
 * If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>
 *
 **********************************************************************************/
class ItemWeaponBow['oBow'] : MapItem;

int arrows = 6;


override bool initialize () {
  if (!::initialize()) return false;
  setSprite('sBowLeft', 'sBowRight');
  return true;
}


// called only if object was succesfully put into player hands
override void onPickedUp (PlayerPawn plr) {
  if (!wasCollected) { wasCollected = true; level.addCollect(objName); }
  global.arrows = min(99, global.arrows+arrows);
  arrows = 0;
}


void armBow (PlayerPawn plr) {
  if (!plr.bowArmed && global.arrows > 0) {
    plr.bowArmed = true;
    plr.playSound('sndBowPull');
  } else if (global.arrows <= 0) {
    level.osdMessage("I'M OUT OF ARROWS!", 2.6);
  }
}


// return `true` to stop player from throwing it
override bool onTryUseItem (PlayerPawn plr) {
  //writeln("BOW USE! kAttack=", plr.kAttack, "; kAttackPressed=", plr.kAttackPressed, "; bowArmed=", plr.bowArmed, "; bowStrength=", plr.bowStrength, "; holdArrow=", plr.holdArrow);

  // put it down?
  /*
  if (plr.scrPlayerIsDucking()) {
    if (plr.kAttackPressed) plr.scrUsePutItemOnGround();
    return true;
  }
  */

  if (plr.firing || plr.scrPlayerIsDucking()) return true;

  if (!plr.bowArmed) {
    armBow(plr);
    return true;
  }

  if (global.arrows <= 0) {
    level.osdMessage("I'M OUT OF ARROWS!", 2.6);
    return true;
  }

  int axdelta, xdir, odir;
  if (plr.dir == Dir.Left) {
    axdelta = -14;
    xdir = -1;
    odir = 180;
  } else {
    axdelta = 14;
    xdir = 1;
    odir = 0;
  }

  MapObject obj = none;
  if (level.isSolidAtPoint(ix+axdelta, iy)) {
    obj = level.MakeMapObject(ix, iy, 'oArrow');
  } else {
    obj = level.MakeMapObject(ix+axdelta, iy, 'oArrow');
  }
  if (!obj) return false;

  obj.xVel = xVel+xdir+plr.bowStrength*xdir;
  if (xdir < 0) {
    if (obj.xVel > -1) obj.xVel = -1;
  } else {
    if (obj.xVel < 1) obj.xVel = 1;
  }

  if (/*checkUp()*/plr.kUp) {
    obj.yVel = -1-plr.bowStrength;
    if (obj.yVel >= -1) obj.yVel = -1;
  } else {
    obj.yVel = 0;
  }

  obj.imageAngle = odir;
  obj.safe = true;
  obj.alarmDisarmSafe = 10;
  plr.playSound('sndArrowTrap');
  plr.firing = 10;
  ItemProjectileArrow(obj).launchedByPlayer = true;

  if (plr.holdArrow == PlayerPawn::ARROW_BOMB) {
    //writeln("BOMB ARROWS!");
    obj.setSprite('sBombArrowRight');
    //!!!obj.alarm[1] = plr.bombArrowCounter;
    ItemProjectileArrow(obj).explosionTimer = plr.bombArrowCounter;
    plr.bombArrowCounter = 80;
    if (global.bombs > 0) global.bombs -= 1; // YASM 1.8.1
  }

  plr.holdArrow = 0;
  --global.arrows;

  auto sname = plr.getSprite().Name;
  if (sname == 'sDuckLeft' || sname == 'sDamselDuckL') obj.flty = obj.iy+4;

  plr.bowArmed = false;
  plr.bowStrength = 0;
  //!!!if (sndIsPlaying(global.sndBowPull)) sndStopSound(global.sndBowPull);
  plr.sndStopSound('sndBowPull');

  return true;
}


override void thinkFrame () {
  if (heldBy) {
    auto plr = PlayerPawn(heldBy);
    /*
    if (plr.dir == DiroPlayer1.facing == 18) sprite_index = sBowLeft;
    else sprite_index = sBowRight;
    */
    if (plr) {
           if (plr.bowStrength >= 10) imageFrame = 3;
      else if (plr.bowStrength > 6) imageFrame = 2;
      else if (plr.bowStrength > 2) imageFrame = 1;
      else imageFrame = 0;
    } else {
      imageFrame = 0;
    }
  } else {
    imageFrame = 0;
    ::thinkFrame();
  }
}


override void drawWithOfs (int xpos, int ypos, int scale, float currFrameDelta) {
  auto plr = PlayerPawn(heldBy);
  if (plr) {
    if (!plr.bowArmed || !plr.holdArrow) {
      imageAngle = 0;
      ::drawWithOfs(xpos, ypos, scale, currFrameDelta);
      return;
    }

    /*
    if (plr.dir == Dir.Left) {
      imageAngle = (plr.kUp ? 180+45 : 180);
    } else {
      imageAngle = (plr.kUp ? -45 : 0);
    }
    ::drawWithOfs(xpos, ypos, scale, currFrameDelta);
    */

    int dy = (plr.kUp ? -4 : -1);
    if (plr.dir == Dir.Left) {
      //imageAngle = (plr.kUp ? 180+45 : 180);
      imageAngle = (plr.kUp ? 45 : 0);
    } else {
      imageAngle = (plr.kUp ? -45 : 0);
    }
    ::drawWithOfs(xpos, ypos, scale, currFrameDelta);

    int xi, yi;
    getInterpCoords(currFrameDelta, scale, out xi, out yi);
    /*
    SpriteImage spr = getSprite();
    if (spr) {
      auto spf = spr.frames[0];
      if (spf && spf.width > 0 && spf.height > 0) spf.blitAt(xi-xpos-spf.xofs*scale, yi-ypos-spf.yofs*scale, scale);
    }
    */
    SpriteImage spr;
    int frm = 0;
    int dx;
    if (plr.dir == Dir.Right) {
      dx = 4;
      if (plr.holdArrow == PlayerPawn::ARROW_NORM) {
        spr = level.sprStore['sArrowRight'];
      } else if (plr.holdArrow == PlayerPawn::ARROW_BOMB) {
        //!!!??if (holdArrowToggle) draw_sprite(sBombArrowRight, 0, x+4, y+2); else draw_sprite(sBombArrowRight, 1, x+4, y+2);
        spr = level.sprStore['sBombArrowRight'];
      }
    } else {
      dx = -4;
      if (plr.holdArrow == PlayerPawn::ARROW_NORM) {
        spr = level.sprStore['sArrowLeft'];
      } else if (plr.holdArrow == PlayerPawn::ARROW_BOMB) {
        //!!!??if (holdArrowToggle) draw_sprite(sBombArrowRight, 0, x+4, y+2); else draw_sprite(sBombArrowRight, 1, x+4, y+2);
        spr = level.sprStore['sBombArrowLeft'];
      }
    }
    if (spr) {
      auto spf = spr.frames[frm];
      if (spf && spf.width > 0 && spf.height > 0) spf.blitAt(xi-xpos-spf.xofs*scale+dx*scale, yi-ypos-spf.yofs*scale+dy*scale, scale, angle:imageAngle);
    }

    imageAngle = 0;
  } else {
    ::drawWithOfs(xpos, ypos, scale, currFrameDelta);
  }
}


defaultproperties {
  objName = 'Bow';
  desc = "Bow";
  desc2 = "A wooden shortbow. It requires a steady supply of arrows to remain functional.";
  shopDesc = "A BOW AND ARROWS";
  setCollisionBounds(-4, -4, 4, 4);
  cost = 1000;
  imageSpeed = 0;

  invincible = true;
  canPickUp = true;
  canHitEnemies = true;

  holdYOfs = 2;
  depth = 92;

  activeWhenHeld = true; // for animation
  spectralWhenHeld = true;

  sellingToShopAllowed = true;
}
