/**********************************************************************************
 * Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC
 * Copyright (c) 2018, Ketmar Dark
 *
 * This file is part of Spelunky.
 *
 * You can redistribute and/or modify Spelunky, including its source code, under
 * the terms of the Spelunky User License.
 *
 * Spelunky is distributed in the hope that it will be entertaining and useful,
 * but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.
 *
 * The Spelunky User License should be available in "Game Information", which
 * can be found in the Resource Explorer, or as an external file called COPYING.
 * If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>
 *
 **********************************************************************************/
class ItemWeaponSceptre['oSceptre'] : MapItem;


override bool initialize () {
  if (!::initialize()) return false;
  setSprite('sSceptreLeft', 'sSceptreRight');
  return true;
}


// called only if object was succesfully put into player hands
override void onPickedUp (PlayerPawn plr) {
  if (!wasCollected) { wasCollected = true; level.addCollect(objName); }
}


// return `true` to stop player from throwing it
override bool onTryUseItem (PlayerPawn plr) {
  // put it down?
  if (plr.scrPlayerIsDucking()) {
    plr.scrUsePutItemOnGround();
    return true;
  }

  if (plr.firing || plr.scrPlayerIsDucking()) return true;

  int xsgn = (plr.dir == Dir.Left ? -1 : 1);
  int xofs = 12*xsgn;

  foreach (; 0..3) {
    auto obj = level.MakeMapObject(ix+xofs, iy+4, 'oPsychicCreateP');
    if (obj) {
      obj.xVel = xsgn*global.randOther(1, 3);
      obj.yVel = -global.randOtherFloat(2);
    }
  }
  auto obj = level.MakeMapObject(ix+xofs, iy-2, 'oPsychicWaveP');
  if (obj) obj.xVel = xsgn*6;
  playSound('sndPsychic');
  plr.firing = PlayerPawn::firingPistolMax;

  return true;
}


defaultproperties {
  objName = 'Sceptre';
  desc = "Sceptre";
  desc2 = "An ancient artifact with great psychic powers."; // "powered by the tortured souls of seventy-two lawyers" wrong tone?
  shopDesc = "A SCEPTRE";
  setCollisionBounds(-4, -4, 4, 4);
  cost = 80000;

  invincible = true;
  canPickUp = true;
  canHitEnemies = true;
  sellingToShopAllowed = true;
  fixedPrice = true;

  holdYOfs = 2;
  depth = 101;
}


// ////////////////////////////////////////////////////////////////////////// //
class ItemSfxPsychicCreate['oPsychicCreateP'] : MapObject;


override bool initialize () {
  if (!::initialize()) return false;
  setSprite('sPsychicCreate');
  return true;
}


override void onAnimationLooped () {
  instanceRemove();
}


override void thinkFrame () {
  setCollisionBoundsFromFrame();
  shiftY(yVel);
  yVel = fmin(yVel+0.6, 6);
  if (isCollision()) {
    instanceRemove();
    return;
  }
}


defaultproperties {
  objName = 'Psychic Wave';
  xVel = 0;
  yVel = 0;
  imageSpeed = 0.2;
  spectral = true;
  depth = 1;
}


// ////////////////////////////////////////////////////////////////////////// //
class ItemProjPsychicWave['oPsychicWaveP'] : MapObject;

float dirAngle;


override bool initialize () {
  if (!::initialize()) return false;
  setSprite('sPsychicWaveP');
  return true;
}


override void onAnimationLooped () {
  instanceRemove();
}


override void thinkFrame () {
  setCollisionBoundsFromFrame();

  if (counter > 0) {
    --counter;
    shiftX(xVel);
    dirAngle = (xVel > 0 ? 0 : 180);
  } else {
    auto enemy = level.findNearestObject(ix, iy, delegate bool (MapObject o) {
      if (o isa EnemyAlienBoss) return false;
      auto dms = MonsterDamsel(o);
      if (dms) {
        //if (e.stunned || e.status >= STATE_STUNNED) return false;
        if (/*dms.dead || dms.status == STATE_DEAD ||*/ !dms.active) return false;
        return true;
      }
      auto enemy = MapEnemy(o);
      if (enemy) {
        //if (e.stunned || e.status >= STATE_STUNNED) return false;
        if (/*enemy.dead || enemy.status == STATE_DEAD ||*/ !enemy.active) return false;
        return true;
      }
      return false;
    }, castClass:MapEnemy);
    if (enemy) dirAngle = pointDirection(ix, iy, enemy.ix+8, enemy.iy+8);
    shiftXY(
      2*cos(dirAngle),
     -2*sin(dirAngle)
    );
  }

  level.isObjectInRect(x0, y0, width, height, delegate bool (MapObject o) {
    if (o isa EnemyAlienBoss) return false;
    if (/*o.dead ||*/ o.invincible) return false;
    // shopkeeper
    auto scp = MonsterShopkeeper(o);
    if (scp && !scp.angered) scp.status = ATTACK;
    // damsel
    auto dms = MonsterDamsel(o);
    if (dms) {
      if (dms.status == STATE_DEAD) return false;
      dms.xVel = global.randOther(0, 2)-global.randOther(1, 2);
      dms.xVel = -1;
      dms.yVel = -6;
      if (dms.hp > 0) {
        dms.hp -= damage;
        dms.spillBlood();
        dms.status = THROWN;
        dms.counter = 120;
        dms.kissCount = min(1, dms.kissCount);
        //playSound('sndDamsel');
      }
      instanceRemove(); //???
      return true;
    }
    // enemy
    auto enemy = MapEnemy(o);
    if (enemy) {
      //if (enemy.status == STATE_DEAD) return false;
      if (enemy.hp > 0) {
        enemy.hp -= damage;
        enemy.spillBlood();
      }
      enemy.xVel = global.randOther(0, 2)-global.randOther(1, 2);
      enemy.xVel = -1;
      enemy.yVel = -6;
      return false;
    }
    return false;
  });
}


defaultproperties {
  objName = 'Psychic Wave';
  damage = 1;
  yVel = 0;
  yAcc = 0.6;
  imageSpeed = 0.25;
  counter = 5;
  dirAngle = 0;
  //dir = Dir.Left;
  depth = 1;
}
