/**********************************************************************************
 * Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC
 * Copyright (c) 2010, Moloch
 * Copyright (c) 2018, Ketmar Dark
 *
 * This file is part of Spelunky.
 *
 * You can redistribute and/or modify Spelunky, including its source code, under
 * the terms of the Spelunky User License.
 *
 * Spelunky is distributed in the hope that it will be entertaining and useful,
 * but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.
 *
 * The Spelunky User License should be available in "Game Information", which
 * can be found in the Resource Explorer, or as an external file called COPYING.
 * If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>
 *
 **********************************************************************************/
// recoded by Ketmar // Invisible Vector
class PlayerPowerup : Object abstract;

name id;
PlayerPawn owner;
int renderPriority; // from lower to higher
bool lostOnDeath = true;
bool active;
bool prePreDucking; // for cape


// called when the player get it for the first time
bool onActivate () {
  active = true;
  return true;
}


// called when the player lost it (for some reason)
bool onDeactivate (optional bool forced) {
  active = false;
  return true;
}


void onPlayerDied () {
  onDeactivate(forced:true);
}


void onPreThink () {
}


void onPostThink () {
}


void prePreDrawWithOfs (int xpos, int ypos, int scale, float currFrameDelta) {
}


void preDrawWithOfs (int xpos, int ypos, int scale, float currFrameDelta) {
}


void postDrawWithOfs (int xpos, int ypos, int scale, float currFrameDelta) {
}


void lastDrawWithOfs (int xpos, int ypos, int scale, float currFrameDelta) {
}


// ////////////////////////////////////////////////////////////////////////// //
class PPCape : PlayerPowerup;

bool visible;
int dx, dy;
name sprName;
bool beforePlayer;
bool open;


// called when the player get it for the first time
override bool onActivate () {
  owner.global.hasCape = true;
  visible = true;
  active = true;
  //open = false;
  return true;
}


// called when the player lost it (for some reason)
override bool onDeactivate (optional bool forced) {
  owner.global.hasCape = false;
  visible = false;
  active = false;
  open = false;
  return true;
}


override void onPostThink () {
  visible = (owner.status != MapObject::DUCKTOHANG);

  if (!owner.whipping && (owner.status == MapObject::CLIMBING || owner.isExitingSprite())) {
    dx = 0;
    dy = 4;
    sprName = 'sCapeBack';
    //depth = 0;
    beforePlayer = true;
  } else if (owner.dir == MapObject::Dir.Right) {
    dx = -4;
    dy = (owner.status == MapObject::DUCKING || owner.stunned ? 2 : -2);
         if (open) sprName = 'sCapeUR';
    else if (fabs(owner.xVel) > 0.4 && owner.status != MapObject::DUCKING) sprName = 'sCapeRight';
    else sprName = 'sCapeDR';
    //depth = 111;
    beforePlayer = false;
  } else {
    // left
    dx = 4;
    dy = (owner.status == MapObject::DUCKING || owner.stunned ? 2 : -2);
         if (open) sprName = 'sCapeUL';
    else if (fabs(owner.xVel) > 0.4 && owner.status != MapObject::DUCKING) sprName = 'sCapeLeft';
    else sprName = 'sCapeDL';
    //depth = 111;
    beforePlayer = false;
  }

  //writeln("CAPE: '", sprName, "'; before=", beforePlayer);
}


final void doDrawWithOfs (int xpos, int ypos, int scale, float currFrameDelta) {
  if (!sprName) return;

  auto spr = owner.level.sprStore[sprName];
  if (!spr || spr.frames.length < 1) return;
  auto spf = spr.frames[0];
  if (!spf || spf.width < 1 || spf.height < 1) return;

  int xi, yi;
  owner.getInterpCoords(currFrameDelta, scale, out xi, out yi);

  xi += dx*scale;
  yi += dy*scale;

  xi -= spf.xofs*scale;
  yi -= spf.yofs*scale;

  spf.blitAt(xi-xpos, yi-ypos, scale);
}


override void prePreDrawWithOfs (int xpos, int ypos, int scale, float currFrameDelta) {
  if (!visible || owner.status != MapObject::DUCKING) return;
  doDrawWithOfs(xpos, ypos, scale, currFrameDelta);
}


override void preDrawWithOfs (int xpos, int ypos, int scale, float currFrameDelta) {
  if (!visible || beforePlayer || owner.status == MapObject::DUCKING) return;
  doDrawWithOfs(xpos, ypos, scale, currFrameDelta);
}


override void lastDrawWithOfs (int xpos, int ypos, int scale, float currFrameDelta) {
  if (!visible || !beforePlayer) return;
  doDrawWithOfs(xpos, ypos, scale, currFrameDelta);
}


defaultproperties {
  id = 'Cape';
  renderPriority = 1010; // behind the bricks
  sprName = 'sCapeRight';
}


// ////////////////////////////////////////////////////////////////////////// //
class PPParachute : PlayerPowerup;

const int dx = -8;
const int dy = -16;
int imageFrame = 0;
bool opening;

// called when the player get it for the first time
override bool onActivate () {
  active = true;
  opening = true;
  imageFrame = 0;
  return true;
}


// called when the player lost it (for some reason)
override bool onDeactivate (optional bool forced) {
  active = false;
  return true;
}


override void onPostThink () {
  if (opening) {
    auto spr = owner.level.sprStore['sParaOpen'];
    ++imageFrame;
    if (imageFrame >= spr.frames.length) {
      opening = false;
      imageFrame = 0;
    }
  } else {
    imageFrame = 0;
  }
}


final void doDrawWithOfs (int xpos, int ypos, int scale, float currFrameDelta) {
  auto spr = owner.level.sprStore[opening ? 'sParaOpen' : 'sParachute'];
  if (!spr || spr.frames.length < 1) return;
  auto spf = spr.frames[imageFrame];
  if (!spf || spf.width < 1 || spf.height < 1) return;

  int xi, yi;
  owner.getInterpCoords(currFrameDelta, scale, out xi, out yi);

  xi += dx*scale;
  yi += dy*scale;

  xi -= spf.xofs*scale;
  yi -= spf.yofs*scale;

  spf.blitAt(xi-xpos, yi-ypos, scale);
}


override void preDrawWithOfs (int xpos, int ypos, int scale, float currFrameDelta) {
  doDrawWithOfs(xpos, ypos, scale, currFrameDelta);
}


defaultproperties {
  id = 'Parachute';
  renderPriority = 1010; // behind the bricks
}
