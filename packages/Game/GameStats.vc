/**********************************************************************************
 * Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC
 * Copyright (c) 2010, Moloch
 * Copyright (c) 2018, Ketmar Dark
 *
 * This file is part of Spelunky.
 *
 * You can redistribute and/or modify Spelunky, including its source code, under
 * the terms of the Spelunky User License.
 *
 * Spelunky is distributed in the hope that it will be entertaining and useful,
 * but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.
 *
 * The Spelunky User License should be available in "Game Information", which
 * can be found in the Resource Explorer, or as an external file called COPYING.
 * If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>
 *
 **********************************************************************************/
// stats: kills, highscores, etc.
class GameStats : Object;

transient GameGlobal global;

// highscore flags
transient bool newMoneyRecord = false;
transient bool newKillsRecord = false;
transient bool newSavesRecord = false;
transient bool newTimeRecord = false;

int frameAccum; // yes, save this
int playingTime; // in seconds

// Tunnel Man Prices
const int tunnel1Price = 100000;
const int tunnel2Price = 200000;
const int tunnel3Price = 300000;

/*
int tunnel1Left = tunnel1Price+1;
int tunnel2Left = tunnel2Price+1;

bool tunnel1Active = false;
bool tunnel2Active = false;
bool tunnel3Active = false;
*/
int[3] tunnelPaymentLeft;
int[3] walksLeftForTunnelMan;


bool isTunnelActive (int idx) {
  return (idx >= 1 && idx <= 3 ? tunnelPaymentLeft[idx-1] <= 0 : false);
}


bool isAnyTunnelActive () {
  return (tunnelPaymentLeft[0] <= 0 || tunnelPaymentLeft[1] <= 0 || tunnelPaymentLeft[2] <= 0);
}


bool needTunnelMan (int transidx) {
  if (transidx < 1 || transidx > 3) return false;
  --transidx;
  if (walksLeftForTunnelMan[transidx] > 0) {
    --walksLeftForTunnelMan[transidx];
    return false;
  }
  return (tunnelPaymentLeft[transidx] > 0);
}


int leftForNextTunnel () {
  foreach (int val; tunnelPaymentLeft) if (val > 0) return val;
  return 0;
}


// returns `true` if new tunnel is built
bool payForTunnel (int donation) {
  if (donation < 1) return false;
  bool res = false;
  foreach (ref int val; tunnelPaymentLeft) {
    if (val > 0) {
      int spend = min(val, donation);
      val -= spend;
      donation -= spend;
      res = res || (val == 0);
    }
  }
  return res;
}


void activateTunnel (int idx) {
  if (idx >= 1 && idx <= 3) {
    --idx;
    tunnelPaymentLeft[idx] = 0;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// indexed by object name
struct TotalItem {
  name objName;
  int count;
}


static final void addTotalCollapsed (ref array!TotalItem totals, name aname, optional int amount) {
  if (!aname) return;
  if (!specified_amount) amount = 1;
  if (amount < 1) return;
  foreach (int idx, ref auto tt; totals) {
    if (tt.objName == aname) {
      ++tt.count;
      return;
    }
  }
  // new object
  totals.length += 1;
  totals[$-1].objName = aname;
  totals[$-1].count = 1;
}


// ////////////////////////////////////////////////////////////////////////// //
// all-time totals, collapsed
array!TotalItem totalDeaths; // death from xxx
array!TotalItem totalKills; // player kills
array!TotalItem totalCollected; // collected items
array!int levelDeaths; // deaths on the corresponding level (1-based)
int totalDamselsSaved;
int totalIdolsStolen;
int totalIdolsConverted; // successfully carried out of level
int totalCrystalIdolsStolen;
int totalCrystalIdolsConverted; // successfully carried out of level
int totalGhostSummoned;
int totalScarabs;
int totalSacrifices;
int totalSelfSacrifices;
int totalDestroyedKaliAltars;
int totalTelefragKills;
int totalDiceGamesWon;
int totalDiceGamesLost;
int totalDiceGamesWonPrize;
// maximum money
int maxMoney = 0;
int gamesWon = 0;
int maxLevelComplete = 0;

int starsKills = 0; // shopkeepers killed

// Minigames
int mini1 = 0;
int mini2 = 0;
int mini3 = 0;
int scoresStart = 0;

int introViewed = 0;


// current game, uncollapsed
struct LevelStatInfo {
  name aname;
  // for transition screen
  bool render;
  int x, y;
}

transient array!LevelStatInfo kills;
transient array!LevelStatInfo collected;
transient int damselsSaved;
transient int idolsStolen;
transient int idolsConverted; // successfully carried out of level
transient int crystalIdolsStolen;
transient int crystalIdolsConverted; // successfully carried out of level
transient int ghostSummoned;
transient int money;
transient int scarabs;

private transient bool moneyCheat;


// ////////////////////////////////////////////////////////////////////////// //
final int countKills () {
  return kills.length;
}


final int getDeathCountOnLevel (int level) {
  if (level < 1 || level >= levelDeaths.length) return 0;
  return levelDeaths[level];
}

final void addDeath (name aname) {
  if (!aname) return;
  addTotalCollapsed(totalDeaths, aname, 1);
  if (global.currLevel > 0 && global.currLevel < 666) {
    if (global.currLevel >= levelDeaths.length) levelDeaths.length = global.currLevel+1;
    ++levelDeaths[global.currLevel];
  }
}

final void addKill (name aname, optional bool telefrag) {
  if (!aname) return;
  if (telefrag) ++totalTelefragKills;
  if (telefrag) {
    addTotalCollapsed(totalKills, name(string(aname)~" Telefrag"), 1);
  } else {
    addTotalCollapsed(totalKills, aname, 1);
  }
  kills.length += 1;
  kills[$-1].aname = aname;
  kills[$-1].render = false;
}

final void addCollect (name aname, optional int amount) {
  if (!aname) return;
  if (!specified_amount) amount = 1;
  if (amount < 1) return;
  addTotalCollapsed(totalCollected, aname, amount);
  collected.length += 1;
  collected[$-1].aname = aname;
  collected[$-1].render = false;
}

final void addDamselSaved () { ++totalDamselsSaved; ++damselsSaved; }
final void addIdolStolen () { ++totalIdolsStolen; ++idolsStolen; }
final void addIdolConverted () { ++totalIdolsConverted; ++idolsConverted; }
final void addCrystalIdolStolen () { ++totalCrystalIdolsStolen; ++crystalIdolsStolen; }
final void addCrystalIdolConverted () { ++totalCrystalIdolsConverted; ++crystalIdolsConverted; }
final void addGhostSummoned () { ++totalGhostSummoned; ++ghostSummoned; }

final void gameOver () {
  if (!moneyCheat && money > 0 && money > maxMoney) {
    newMoneyRecord = true;
    maxMoney = money;
  }
}

final void setMoneyCheat () { moneyCheat = true; }

final void setMoney (int newmoney) {
  newmoney = max(0, newmoney);
  money = newmoney;
}

final void addMoney (int amount) {
  setMoney(money+amount);
}

final void takeMoney (int amount) {
  setMoney(money-amount);
}

final void oneMoreFramePlayed () {
  if (++frameAccum >= 30) {
    playingTime += frameAccum/30;
    frameAccum %= 30;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
final void clearLevelTotals () {
  kills.clear();
  collected.clear();
}


// ////////////////////////////////////////////////////////////////////////// //
final void clearGameTotals () {
  clearLevelTotals();
  damselsSaved = 0;
  idolsStolen = 0;
  idolsConverted = 0;
  crystalIdolsStolen = 0;
  crystalIdolsConverted = 0;
  ghostSummoned = 0;
  money = 0;
  scarabs = 0;
  newMoneyRecord = false;
  moneyCheat = false;
}


// ////////////////////////////////////////////////////////////////////////// //
void resetTunnelPrices () {
  tunnelPaymentLeft[0] = tunnel1Price;
  tunnelPaymentLeft[1] = tunnel2Price;
  tunnelPaymentLeft[2] = tunnel3Price;
  walksLeftForTunnelMan[0] = 0;
  walksLeftForTunnelMan[1] = 0;
  walksLeftForTunnelMan[2] = 0;
}



// ////////////////////////////////////////////////////////////////////////// //
defaultproperties {
  tunnelPaymentLeft[0] = tunnel1Price;
  tunnelPaymentLeft[1] = tunnel2Price;
  tunnelPaymentLeft[2] = tunnel3Price;
  walksLeftForTunnelMan[0] = 3;
  walksLeftForTunnelMan[1] = 2;
  walksLeftForTunnelMan[2] = 1;
}
