/**********************************************************************************
 * Copyright (c) 2008, 2009 Derek Yu and Mossmouth, LLC
 * Copyright (c) 2010, Moloch
 * Copyright (c) 2018, Ketmar Dark
 *
 * This file is part of Spelunky.
 *
 * You can redistribute and/or modify Spelunky, including its source code, under
 * the terms of the Spelunky User License.
 *
 * Spelunky is distributed in the hope that it will be entertaining and useful,
 * but WITHOUT WARRANTY.  Please see the Spelunky User License for more details.
 *
 * The Spelunky User License should be available in "Game Information", which
 * can be found in the Resource Explorer, or as an external file called COPYING.
 * If not, please obtain a new copy of Spelunky from <http://spelunkyworld.com/>
 *
 **********************************************************************************/
class EntityGenJungleBizarre : EntityGen transient;

transient bool ashGraveGenerated;


// Note: depth of trees, statues is 9005
override void generateEntities () {
  global = levgen.global;
  level = levgen.level;

  // level type 1 (jungle)
  if (global.cemetary) {
    ashGraveGenerated = false;
    level.forEachSolidTileOnGrid(delegate bool (int tileX, int tileY, MapTile t) {
      if (t.objType != 'oLush') return false;
      // generate graves
      int x = tileX*16, y = tileY*16;
      if (level.checkTileAtPoint(x, y-16, delegate bool (MapTile t) { return (t.solid || t.enter || t.exit); })) return false;
      if (!(x != 160 && x != 176 && x != 320 && x != 336 && x != 480 && x != 496)) return false;
      if (global.randRoom(1, 20) != 1) return false;
      //MapTile obj;
      if (!ashGraveGenerated && global.randRoom(1, 30) == 1) {
        /*obj =*/ level.MakeMapTile(tileX, tileY-1, 'oAshGrave');
        ashGraveGenerated = true;
      } else {
        /*obj =*/ level.MakeMapTile(tileX, tileY-1, 'oGrave');
      }
      return false;
    });
  }

  level.forEachSolidTileOnGrid(delegate bool (int tileX, int tileY, MapTile t) {
    auto rg = levgen.getRoomGenForTileAt(tileX, tileY, allowBorders:false);
    if (rg) rg.genEntityAt(tileX, tileY);
    return false;
  });

  /+
  // force market entrance
  if (global.genMarketEntrance && !global.madeMarketEntrance) {
    with (oSolid) {
      if (y > 32 && collision_point(x, y-16, oSolid, 0, 0)) {
        obj = instance_place(x, y-16, oSolid);
        if (obj.type != "Tree" && type != "Altar" && !obj.invincible) {
          MakeMapObject(x, y-16, 'oXMarket');
          invincible = true;
          global.madeMarketEntrance = true;
        }
      }
    }
  }

  with (oVine) {
    if (global.randRoom(1, fceil(15.0/global.config.enemyMult)) == 1) MakeMapObject(x, y, 'oMonkey');
  }

  with (oWater) {
    if (!collision_point(x, y, oSolid, 0, 0)) {
      if (global.randRoom(1, fceil(30.0/fceil(global.config.enemyMult/2.0))) == 1) {
          if (global.cemetary) MakeMapObject(x+4, y+4, 'oDeadFish'); else MakeMapObject(x+4, y+4, 'oPiranha');
      }
    }
  }
  +/
}


defaultproperties {
  rgLevelType = 666; // not ready yet
  rgBizarre = true;
}
